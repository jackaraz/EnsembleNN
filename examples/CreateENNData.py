#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 21:40:38 2019

@author: jackaraz
"""

import numpy as np
import pandas as pd
import os, time, sys

from skhep.math.vectors import LorentzVector
from fastjet import *

from EnsembleNN.preprocessing.RNN_preprocessing.utils   import kTdistances_cluster_history, get_parents
from EnsembleNN.preprocessing.CNN_preprocessing.utils   import preprocess
from EnsembleNN.system.feedback                         import progress

"""
    rearranging data from 
        https://zenodo.org/record/2603256#.Xjnnty_MzRZ
"""

main_path    = '/mt/batch/jaraz/RNN_CNN'
dataset_path = os.path.join(main_path,'dataset/v0')

sample_sets = [x for x in sys.argv if x in ['train','val','test']]
data_ID = {0:'QCD',1:'tt'}

debug=False
if any([(x in ['-d','--debug']) for x in sys.argv]):
    debug = True

lstm_shape  = 40
image_shape = (1,37,37)


if sample_sets == []:
    sample_sets = ['train','val','test']

print '   * Running over the sample for '+', '.join([y for x,y in data_ID.items()])
print '   * Following sample sets will be used: '+', '.join(sample_sets)

store = -1
start_time= time.time()
for dataset in sample_sets:
    if not os.path.isfile(os.path.join(dataset_path,dataset+'.h5')):
        raise Warning( os.path.join(dataset_path,dataset+'.h5')+' does not exist.')

    if isinstance(store,pd.HDFStore):
        if store.is_open:
            store.close()
    store    = pd.HDFStore(os.path.join(dataset_path,dataset+'.h5'))
    iterator = store.select('table',iterator=True,chunksize=5000, auto_close=True)

    print '   * Sample size = ', iterator.nrows

    n = 0
    report_events = 0

    # initialize containers
    smp_top,smp_qcd   = 1,1
    to_save_lstm_top  = np.zeros(lstm_shape)
    to_save_image_top = np.zeros(image_shape)
    to_save_lstm_qcd  = np.zeros(lstm_shape)
    to_save_image_qcd = np.zeros(image_shape)

    for data in iterator:
        data = data.reset_index(drop=True)
        IDs  = data['is_signal_new']
        data = data.drop(columns=[i for i in data.keys() if not (i.startswith('E') or i.startswith('P'))])
        if len(data)==0:
            continue

        ix = range(data.shape[0])
        for evtnum in ix:
            current_event = data_ID[IDs[evtnum]]

            report_events+=(n%100==0 and n>0)*100
            progress(n, iterator.nrows, status=dataset+' -> Evt>='+str(report_events),\
                     start_time=start_time)
            n+=1

            particles = []
            for p in range(200):
                momentum = LorentzVector(data.iloc[ix[evtnum]]['PX_'+str(p)],
                                         data.iloc[ix[evtnum]]['PY_'+str(p)],
                                         data.iloc[ix[evtnum]]['PZ_'+str(p)],
                                         data.iloc[ix[evtnum]]['E_'+str(p)])
                if momentum.pt>0:
                    pseudo = PseudoJet(np.double(momentum.px), np.double(momentum.py),
                                       np.double(momentum.pz), np.double(momentum.e))
                    pseudo.set_python_info(1)
                    particles.append(pseudo) # px, py, pz, E


            # Cluster particles with antikT algo R = 0.8
            # find the hardest jet
            R         = 0.8
            jet_def   = JetDefinition(antikt_algorithm, R)
            clust_seq = ClusterSequence(particles, jet_def)
            jets      = sorted_by_pt(clust_seq.inclusive_jets(20.))
            hard_jet  = jets[0]

            ### recluster the hardest jet with C/A
            jet_def_hard   = JetDefinition(cambridge_algorithm, R)
            clust_seq_hard = ClusterSequence(hard_jet.constituents(),jet_def_hard)
            ca_jet         = sorted_by_pt(clust_seq_hard.inclusive_jets(20.))
            if len(ca_jet) == 0:
                continue

            """
                            CovNET Data Preparation
            """
            # Take the constituents of the antikT hard jet, the format needs to be
            # [ [[pt1,pt2...]], [[eta1,eta2...]], [[phi1, phi2...]] ]
            # double list is incase there is more than one event, since this
            # code iterating event-by-event basis there is no need to add another 
            # set of subjets
            subjet_pT, subjet_eta, subjet_phi = [],[],[]
            hard_jet_constituents_PTordered = sorted_by_pt(hard_jet.constituents())
            subjets = [[[const.pt()  for const in hard_jet_constituents_PTordered]],
                       [[const.eta() for const in hard_jet_constituents_PTordered]],
                       [[const.phi() for const in hard_jet_constituents_PTordered]]]


            """
                            LSTM Data Preparation
            """
            # take the list of the ktdistance of the 3 leading branch
            if debug: print('C/A Jet pt = {:.3e} GeV'.format(ca_jet[0].pt()))
            branch_jet_1, branch_jet_2, kTdist1 = get_parents(ca_jet[0],clust_seq_hard)
            if debug: print('Leading branch pt = {:.3e} GeV'.format(branch_jet_1.pt()))
            _ ,           branch_jet_3, kTdist2 = get_parents(branch_jet_1,clust_seq_hard)

            if debug: print('2nd Leading branch pt = {:.3e} GeV'.format(branch_jet_2.pt()))
            if debug: print('3rd Leading branch pt = {:.3e} GeV'.format(branch_jet_3.pt()))

            listkT_branch1 = [kTdist1]+kTdistances_cluster_history(branch_jet_1,
                                                                   clust_seq_hard,     cut=0.01)
            listkT_branch2 = kTdistances_cluster_history(branch_jet_2, clust_seq_hard, cut=0.01)
            listkT_branch3 = kTdistances_cluster_history(branch_jet_3, clust_seq_hard, cut=0.01)

            if len(listkT_branch1) < 18:
                listkT_branch1 += [0.]*(18-len(listkT_branch1))
            if len(listkT_branch2) < 10:
                listkT_branch2 += [0.]*(10-len(listkT_branch2))
            if len(listkT_branch3) < 10:
                listkT_branch3 += [0.]*(10-len(listkT_branch3))

            listkT = listkT_branch1[:18]+listkT_branch2[:10]+listkT_branch3[:10]

            """
            MassDrop Tagger
            0. start from a jet obtained from with the Cambridge/Aachen algorithm
            1. undo the last step of the clustering step j -> j1 + j2 (label them 
            such as j1 is the most massive).
            2. if there is a mass drop, i.e. m_j1/m_j < mu_cut, and the splitting 
            is sufficiently symmetric, 
            ${\rm min}(p_{tj1}^2,p_{tj2}^2)\Delta R_{j1,j2}^2 > y_{\rm cut} m_j^2$, 
            keep j as the result of the tagger (with j1 and j2 its 2 subjets)
            3. otherwise, redefine j to be equal to j1 and return to step 1.
            """
            # apply massdrop to the hardest CA jet
            mdt      = MassDropTagger(0.8, 0.09)
            mdt_hard = mdt.result(ca_jet[0])
            
            # Binning all the data
            # output_list = []
            # for out in [hard_jet.pt(),hard_jet.m(),mdt_hard.m()]+listkT[:37]:
            #     try:
            #         if out >= 0:
            #             upper_limmit = 1000
            #             while out >= upper_limmit:
            #                 upper_limmit *= 5
            #             binned_out = np.histogram(out,bins=upper_limmit//5,range=(0,upper_limmit))
            #             binned_out = float(binned_out[1][:-1][binned_out[0]==1])
            #             # position the output to the bin center
            #             output_list.append(binned_out+2.5)
            #         else:
            #             lower_limmit = -1000
            #             while out <= lower_limmit:
            #                 lower_limmit *= 5
            #             binned_out = np.histogram(out,bins=abs(lower_limmit)//5,range=(lower_limmit,0))
            #             binned_out = float(binned_out[1][:-1][binned_out[0]==1])
            #             output_list.append(binned_out-2.5)
            #     except:
            #         raise ValueError('Source of the problem -> {}'.format(out))

            current_out = np.array([hard_jet.m(),mdt_hard.m()]+listkT)

            if current_event == 'tt':
                to_save_lstm_top  = np.vstack((to_save_lstm_top,current_out))
                to_save_image_top = np.vstack((to_save_image_top, preprocess(subjets)))
            elif current_event == 'QCD':
                to_save_lstm_qcd  = np.vstack((to_save_lstm_qcd,current_out))
                to_save_image_qcd = np.vstack((to_save_image_qcd, preprocess(subjets)))


            # Save Top signal
            batch_size = 5000
            if n>=iterator.nrows or len(to_save_lstm_top[1:]) >= 5000:
                to_save_lstm_top  = to_save_lstm_top[1:]
                to_save_image_top = to_save_image_top[1:]
                for isave in range(to_save_lstm_top[1:].shape[0]//batch_size+1):
                    sample_to_save_lstm  = to_save_lstm_top[isave*batch_size:(isave+1)*batch_size]
                    sample_to_save_image = to_save_image_top[isave*batch_size:(isave+1)*batch_size]
                    if sample_to_save_lstm.shape[0]==0 or sample_to_save_image.shape[0] ==0:
                        continue
                    np.savez_compressed(os.path.join(main_path,'EnsembleNN','samples_v2',
                                                     dataset,'tt_LSTM_{:04d}.npz'.format(smp_top)),
                                        sample=sample_to_save_lstm, shape=sample_to_save_lstm.shape)
                    np.savez_compressed(os.path.join(main_path,'EnsembleNN','samples_v2',
                                                     dataset,'tt_CovNet_{:04d}.npz'.format(smp_top)),
                                        sample=sample_to_save_image, shape=np.shape(sample_to_save_image))
                    smp_top+=1
                # clean the containers
                to_save_lstm_top  = np.zeros(lstm_shape)
                to_save_image_top = np.zeros(image_shape)
                del sample_to_save_lstm, sample_to_save_image


            # Save QCD background
            if n>=iterator.nrows or len(to_save_lstm_qcd[1:]) >= 5000:
                to_save_lstm_qcd  = to_save_lstm_qcd[1:]
                to_save_image_qcd = to_save_image_qcd[1:]
                for isave in range(to_save_lstm_qcd[1:].shape[0]//batch_size+1):
                    sample_to_save_lstm  = to_save_lstm_qcd[isave*batch_size:(isave+1)*batch_size]
                    sample_to_save_image = to_save_image_qcd[isave*batch_size:(isave+1)*batch_size]
                    if sample_to_save_lstm.shape[0]==0 or sample_to_save_image.shape[0] ==0:
                        continue
                    np.savez_compressed(os.path.join(main_path,'EnsembleNN','samples_v2',
                                                     dataset,'QCD_LSTM_{:04d}.npz'.format(smp_qcd)),
                                        sample=sample_to_save_lstm, shape=sample_to_save_lstm.shape)
                    np.savez_compressed(os.path.join(main_path,'EnsembleNN','samples_v2',
                                                     dataset,'QCD_CovNet_{:04d}.npz'.format(smp_qcd)),
                                        sample=sample_to_save_image, shape=np.shape(sample_to_save_image))
                    smp_qcd+=1
                # clean the containers
                to_save_lstm_qcd  = np.zeros(lstm_shape)
                to_save_image_qcd = np.zeros(image_shape)
                del sample_to_save_lstm, sample_to_save_image

