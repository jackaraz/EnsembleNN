#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 16:40:46 2021

@author : jackaraz
@contact: Jack Y. Araz <jackaraz@gmail.com>
"""
import numpy  as np
import pandas as pd

from glob import glob as glb
import json, os, argparse, copy

class InvalidPath(Exception):
    def __init__(self,message):
        super(InvalidPath, self).__init__("Invalid Path! "+message)

# ================================= #
# ==== Set the Input Arguments ==== #
# ================================= #

architectures = ["rnn","cnn","brnn","bcnn","benn","benn2"]

parser = argparse.ArgumentParser(
    description="Run specific network architecture and save weights, train history into a designated folder."
)

modelgroup = parser.add_argument_group("Model handling")

modelgroup.add_argument("-m", "--model", default=None, dest="model", required=True, type=str,
    help="Model to be used during training. Available models are " + ", ".join(architectures),
    choices=architectures)
modelgroup.add_argument("-id", "--name", default="Model", dest="name", type=str, 
                        help="Name of the training")

hyperparameters = parser.add_argument_group("Hyper-parameters")
hyperparameters.add_argument("-e", "--epochs", type=int, dest="epochs", default=500,
                             help="Number of epochs")
hyperparameters.add_argument("-lr", "--learning-rate", type=float, dest="lr", default=1e-4, 
                             help="Learning rate")
hyperparameters.add_argument("-q", "--queue", type=int, dest="queue", default=15,
                             help="How many batches to be stored on RAM in the queue")
hyperparameters.add_argument( "-p", "--patience", type=int, dest="patience", default=20,
                             help="Patience for the learning rate decay")
hyperparameters.add_argument( "-tb", "--train_batch", type=int, dest="train_batch", 
                             default=128, help="Batch size for the training sample")
hyperparameters.add_argument( "-vb", "--val_batch", type=int, dest="val_batch", default=128,
                             help="Batch size for the validation sample")
hyperparameters.add_argument( "-nt", "--ntrain", type=int, dest="ntrain", default=122,
                             help="Number of files to be included during training")


paths = parser.add_argument_group("Paths")
paths.add_argument( "--data-path", type=str, dest="data_path",
                    default="/mt/batch/jaraz/RNN_CNN/EnsembleNN/samples_v2",
                    help="Location of the training, validation and test data.")
paths.add_argument("--out-path", type=str, dest="output_path", default="output",
                   help="Output location.")

args = parser.parse_args()

if not os.path.isdir(args.output_path):
    os.mkdir(args.output_path)
if not os.path.isdir(os.path.join(args.output_path,args.model)):
    os.mkdir(os.path.join(args.output_path,args.model))

n_folder = [int(x.split('_')[1]) 
            for x in glb(os.path.join(args.output_path,  args.model, args.name+'_*'))]
n_folder = 0 if n_folder == [] else max(n_folder)+1
output_path = os.path.join(args.output_path, args.model, args.name+'_'+str(n_folder))

if not os.path.isdir(output_path):
    os.mkdir(output_path)

print('   * NN running for '+args.model)
print('      - Learning rate          = {:.2e}'.format(args.lr))
print('      - Number of epoch        = {}'.format(args.epochs))
print('      - Max Queue Size         = {}'.format(args.queue))
print('      - Patience to reduce LR  = {}'.format(args.patience))
print('      - Training sample size   = {}'.format(args.ntrain))
print('   * Output Path : '+output_path)

if 'cnn' in args.model:
    from EnsembleNN.Convolutional.DataGenerator import DataGenerator 
    if args.model == 'cnn':
        from EnsembleNN.Models.DeepTopXS        import DeepTopXS  as model
    elif args.model == 'bcnn':
        from EnsembleNN.Models.simpleBCNN       import BayesModel
elif 'rnn' in args.model:
    from EnsembleNN.Recurrent.DataGenerator import DataGenerator 
    if args.model == 'rnn':
        from EnsembleNN.Models.LSTMArchi    import LSTM_Model as model
    elif args.model == 'brnn':
        from EnsembleNN.Models.simpleBRNN   import BayesModel
elif 'enn' in args.model:
    from EnsembleNN.Ensemble.DataGenerator  import DataGenerator
    if args.model == 'enn':
        from EnsembleNN.Models.EnsembleNN   import Ensemble as model
    elif args.model == 'benn':
        from EnsembleNN.Models.simpleBENN   import BayesModel
    elif args.model == 'benn2':
        from EnsembleNN.Models.simpleBENN_singlelayer  import BayesModel

# ========================= #
# ==== Prepare Samples ==== #
# ========================= #
if not os.path.isdir(args.data_path):
    raise InvalidPath(args.data_path)

train_dict = {'image': {'bkg' : glb(args.data_path+'/train/QCD_CovNet_*'),
                        'sig' : glb(args.data_path+'/train/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(args.data_path+'/train/QCD_LSTM_*'),
                        'sig' : glb(args.data_path+'/train/tt_LSTM_*')}}

val_dict   = {'image': {'bkg' : glb(args.data_path+'/val/QCD_CovNet_*'),
                        'sig' : glb(args.data_path+'/val/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(args.data_path+'/val/QCD_LSTM_*'),
                        'sig' : glb(args.data_path+'/val/tt_LSTM_*')}}

test_dict   = {'image': {'bkg' : glb(args.data_path+'/test/QCD_CovNet_*'),
                         'sig' : glb(args.data_path+'/test/tt_CovNet_*')},
               'lstm' : {'bkg' : glb(args.data_path+'/test/QCD_LSTM_*'),
                         'sig' : glb(args.data_path+'/test/tt_LSTM_*')}}

sort = lambda x : int(x.split('_')[-1].split('.')[0])

for dc in [train_dict, val_dict, test_dict]:
    dc['image']['bkg'].sort(key=sort)
    dc['image']['sig'].sort(key=sort)
    dc['lstm' ]['bkg'].sort(key=sort)
    dc['lstm' ]['sig'].sort(key=sort)

full_training_sample = copy.deepcopy(train_dict)

train_dict['image']['bkg'] = train_dict['image']['bkg'][:args.ntrain]
train_dict['image']['sig'] = train_dict['image']['sig'][:args.ntrain]
train_dict['lstm']['bkg' ] = train_dict['lstm']['bkg' ][:args.ntrain]
train_dict['lstm']['sig' ] = train_dict['lstm']['sig' ][:args.ntrain]

class lstm_std:
    def __init__(self,train_dict):
        from sklearn.preprocessing import RobustScaler 
        # minmaxscaler ruins the outliers which are crutial for kT distances
        X = np.zeros(40)
        i=0
        while X.shape[0]<100000 and i<len(full_training_sample['lstm']['sig']):
            sig, bkg = list(zip(full_training_sample['lstm']['sig'],
                                full_training_sample['lstm']['bkg']))[i]
            sig = np.load(sig)['sample']
            bkg = np.load(bkg)['sample']
            X = np.vstack((X,sig))
            X = np.vstack((X,bkg))
            i+=1
        X = X[1:]
        np.random.shuffle(X)
        self.transformer = RobustScaler().fit(X)
    def __call__(self,X):
        return self.transformer.transform(X)

normalize_meanstd = lambda image : image/1000.

if 'rnn' in args.model:
    lstm_std = lstm_std(train_dict)
    train = DataGenerator(train_dict,batch_size=args.train_batch,std=lstm_std)
    val   = DataGenerator(val_dict,  batch_size=args.val_batch,std=lstm_std)

elif 'cnn' in args.model:
    train = DataGenerator(train_dict,batch_size=args.train_batch,   std=normalize_meanstd)
    val   = DataGenerator(val_dict,  batch_size=args.val_batch,   std=normalize_meanstd)

elif 'enn' in args.model:
    lstm_std = lstm_std(train_dict)
    train = DataGenerator(train_dict,batch_size=args.train_batch,
                          std=[normalize_meanstd,lstm_std])
    val   = DataGenerator(val_dict,  batch_size=args.val_batch  ,
                          std=[normalize_meanstd,lstm_std])

# ================== #
# ==== NN Model ==== #
# ================== #

if args.model.startswith('b'):
    model = BayesModel(train.sample_size)

from tensorflow.keras.callbacks import ( ReduceLROnPlateau, TerminateOnNaN, 
                                        ModelCheckpoint, EarlyStopping )
from tensorflow.keras           import optimizers

adam = optimizers.Adam(learning_rate=args.lr)

model.compile(loss='binary_crossentropy', optimizer=adam,metrics=['accuracy','mse'] )
model.summary()

summary = []
write_summary = lambda x : summary.append(x+'\n')

model.summary(print_fn=write_summary)
summary.append(f"\n\nNumber of training samples used = {len(train_dict['image']['bkg'])*2}\n")

summary_file = open(output_path+'/model_summary.txt','w')
summary_file.writelines(summary)
summary_file.close()

open(output_path+'/model.json','w').write(json.dumps(model.to_json(indent = 4)))

# ============================ #
# ==== Fit Neural Network ==== #
# ============================ #

print('   * Training begins...')
try:
    model.fit(train,
              epochs=args.epochs,
              verbose=1,
              validation_data = val,
              workers = 4,
              # use_multiprocessing = True,
              validation_freq = 1,
              max_queue_size=args.queue,#max_queue_size,  # defaults to 10
              callbacks = [
                        ModelCheckpoint(output_path+'/'+args.model+'_model.h5', 
                                        monitor='val_loss', verbose=1, save_best_only=True),
                        EarlyStopping(verbose=1, patience=args.epochs//2, monitor='val_loss', 
                                      mode='min',min_delta=0.01),
                        ReduceLROnPlateau(monitor='val_loss', factor=0.5,
                                          patience=args.patience, verbose=1, 
                                          mode='min', min_delta=0.01, 
                                          cooldown=0, min_lr=1e-10),
                        TerminateOnNaN()
                   ]
              )
except KeyboardInterrupt:
    print('   * Training stopped by the user.')

train.Reset()
val.Reset()

# save model history
model_history = pd.DataFrame.from_dict(model.history.history)
model_history.to_csv(output_path+'/model_history.csv')
