#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 09:19:57 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy  as np
import pandas as pd

from tensorflow.keras.metrics       import AUC, Precision, KLDivergence
from tensorflow.keras.callbacks     import ReduceLROnPlateau, TerminateOnNaN, ModelCheckpoint, EarlyStopping
from tensorflow.keras.utils         import plot_model
from tensorflow.keras               import optimizers

from sklearn.metrics                import roc_curve, auc

from EnsembleNN.Convolutional.utils import plot_correlations, get_error_correlations

import matplotlib.pyplot as plt
from glob import glob    as glb
import json, sys, os

# ================================= #
# ==== Set the Input Arguments ==== #
# ================================= #

args = sys.argv

#list of available architectures 
architectures = ['lstm','lstmv2','lstmv3', 'semibrnn','simplebrnn',
                 'covnet','bayescnn','simplebcnn',
                 'enn','ennv2','ennv3','ennv4','ennv5','semibenn','simplebenn']

to_run    = [x.lower() for x in args if x.lower() in architectures]
epoch     = [int(x.split('=')[1])   for x in args if any([(y in x) for y in ['-e=','--e=','--epoch=','--epochs=']])]
lr        = [float(x.split('=')[1]) for x in args if any([(y in x) for y in ['-lr=','--lr=','--learning-rate=']])]
queue     = [int(x.split('=')[1])   for x in args if any([(y in x) for y in ['-q=','--q=','--queue=']])]
patience  = [int(x.split('=')[1])   for x in args if any([(y in x) for y in ['-p=','--p=','--patience=']])]
name      = [str(x.split('=')[1])   for x in args if any([(y in x) for y in ['-n=','--n=','--name=']])]
# cont      = any([(y in args)        for y in ['-c','--continue']])

# to_run    = to_run[0]    if len(to_run)>0    else 'enn'
epoch     = epoch[0]     if len(epoch)>0     else 500
lr        = lr[0]        if len(lr)>0        else 1e-4
queue     = queue[0]     if len(queue)>0     else 15
patience  = patience[0]  if len(patience)>0  else 20
name      = name[0]      if len(name)>0      else 'Model'

if len(to_run) == 0:
    print('Please choose a model to run : '+', '.join(architectures))
    sys.exit(0)
else:
    to_run = to_run[0]


print('   * NN running for '+to_run)
print('      - Learning rate          = {:.2e}'.format(lr))
print('      - Number of epoch        = {}'.format(epoch))
print('      - Max Queue Size         = {}'.format(queue))
print('      - Patience to reduce LR  = {}'.format(patience))

# if cont: print('      - Continue from the previous training.')
if to_run == 'covnet':
    from EnsembleNN.Convolutional.DataGenerator import DataGenerator 
    from EnsembleNN.Models.DeepTopXS            import DeepTopXS  as model

elif 'lstm' in to_run:
    from EnsembleNN.Recurrent.DataGenerator import DataGenerator 
    if to_run == 'lstm':
        from EnsembleNN.Models.LSTMArchi    import LSTM_Model as model
    elif 'v2' in to_run:
        from EnsembleNN.Models.LSTMArchiv2  import LSTM_Model as model
    elif 'v3' in to_run:
        from EnsembleNN.Models.LSTMArchiv3  import LSTM_Model as model

elif 'enn' in to_run:
    from EnsembleNN.Ensemble.DataGenerator  import DataGenerator
    if to_run == 'enn':
        from EnsembleNN.Models.EnsembleNN   import Ensemble as model
    elif 'v2' in to_run:
        from EnsembleNN.Models.EnsembleNNv2 import Ensemble as model
    elif 'v3' in to_run:
        from EnsembleNN.Models.EnsembleNNv3 import Ensemble as model
    elif 'v4' in to_run:
        from EnsembleNN.Models.EnsembleNNv4 import Ensemble as model
    elif 'v5' in to_run:
        from EnsembleNN.Models.EnsembleNNv5 import Ensemble as model
    elif 'simple' in to_run:
        from EnsembleNN.Models.simpleBENN   import BayesModel
    elif 'benn' in to_run:
        from EnsembleNN.Models.semiBENN     import BayesModel

elif 'bayescnn' in to_run or 'bcnn' in to_run:
    from EnsembleNN.Convolutional.DataGenerator import DataGenerator 
    if 'simple' in to_run:
        from EnsembleNN.Models.simpleBCNN       import BayesModel
    else:
        from EnsembleNN.Models.BCNN             import BayesModel

elif 'brnn' in to_run:
    from EnsembleNN.Recurrent.DataGenerator import DataGenerator 
    if 'simple' in to_run:
        from EnsembleNN.Models.simpleBRNN   import BayesModel
    else:
        from EnsembleNN.Models.semiBRNN     import BayesModel



folder_name = to_run
if 'enn' in to_run:
    folder_name = 'enn'
elif 'lstm' in to_run or 'rnn' in to_run:
    folder_name = 'lstm'
elif 'cnn' in to_run or 'covnet' in to_run:
    folder_name = 'covnet'

if not os.path.isdir('output/'+folder_name):
    os.mkdir('output/'+folder_name)
n_folder = [int(x.split('_')[1]) for x in glb('output/'+folder_name+'/'+name+'_*')]
if n_folder == []:
    n_folder = 0
else:
    n_folder = max(n_folder)+1

output_path = 'output/'+folder_name+'/'+name+'_'+str(n_folder)
if not os.path.isdir(output_path):
    os.mkdir(output_path)

print('   * Output Path : '+output_path)

# ========================= #
# ==== Prepare Samples ==== #
# ========================= #

sort = lambda x : int(x.split('_')[-1].split('.')[0])

path = '/mt/batch/jaraz/RNN_CNN/EnsembleNN/samples_v2'
train_dict = {'image': {'bkg' : glb(path+'/train/QCD_CovNet_*'),
                        'sig' : glb(path+'/train/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(path+'/train/QCD_LSTM_*'),
                        'sig' : glb(path+'/train/tt_LSTM_*')}}

val_dict   = {'image': {'bkg' : glb(path+'/val/QCD_CovNet_*'),
                        'sig' : glb(path+'/val/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(path+'/val/QCD_LSTM_*'),
                        'sig' : glb(path+'/val/tt_LSTM_*')}}

test_dict   = {'image': {'bkg' : glb(path+'/test/QCD_CovNet_*'),
                         'sig' : glb(path+'/test/tt_CovNet_*')},
               'lstm' : {'bkg' : glb(path+'/test/QCD_LSTM_*'),
                         'sig' : glb(path+'/test/tt_LSTM_*')}}

for dc in [train_dict, val_dict, test_dict]:
    dc['image']['bkg'].sort(key=sort)
    dc['image']['sig'].sort(key=sort)
    dc['lstm' ]['bkg'].sort(key=sort)
    dc['lstm' ]['sig'].sort(key=sort)


class lstm_std:
    def __init__(self,train_dict):
        from sklearn.preprocessing import RobustScaler 
        # minmaxscaler ruins the outliers which are crutial for kT distances
        X = np.zeros(40)
        i=0
        while X.shape[0]<100000 and i<len(train_dict['lstm']['sig']):
            sig, bkg = list(zip(train_dict['lstm']['sig'],train_dict['lstm']['bkg']))[i]
            sig = np.load(sig)['sample']
            bkg = np.load(bkg)['sample']
            X = np.vstack((X,sig))
            X = np.vstack((X,bkg))
            i+=1
        X = X[1:]
        np.random.shuffle(X)
        self.transformer = RobustScaler().fit(X)#
    def __call__(self,X):
        return self.transformer.transform(X)


normalize_meanstd = lambda image : image/1000.


train_batch = 128
val_vatch   = 128

if to_run in ['covnet', 'bayescnn'] or 'bcnn' in to_run:
    train = DataGenerator(train_dict,batch_size=train_batch,   std=normalize_meanstd)
    val   = DataGenerator(val_dict,  batch_size=val_vatch,   std=normalize_meanstd)
elif 'lstm' in to_run or 'rnn' in to_run:
    lstm_std = lstm_std(train_dict)
    train = DataGenerator(train_dict,batch_size=train_batch,std=lstm_std)
    val   = DataGenerator(val_dict,  batch_size=val_vatch,std=lstm_std)
elif 'enn' in to_run:
    lstm_std = lstm_std(train_dict)
    train = DataGenerator(train_dict,batch_size=train_batch,std=[normalize_meanstd,lstm_std])
    val   = DataGenerator(val_dict,  batch_size=val_vatch  ,std=[normalize_meanstd,lstm_std])


# ================== #
# ==== NN Model ==== #
# ================== #

if 'bayes' in to_run or 'brnn' in to_run or 'benn' in to_run or 'bcnn' in to_run:
    model = BayesModel(train.sample_size)

adam = optimizers.Adam(learning_rate=lr)

model.compile(loss='binary_crossentropy', 
              optimizer=adam,metrics=['accuracy','mse',
                                      AUC(name='auc'),
                                      Precision(name='precision'),
                                      KLDivergence(name='kldiv')
                                      ]
              )
model.summary()

summary = []
write_summary = lambda x : summary.append(x+'\n')

summary_file = open(output_path+'/model_summary.txt','w')
model.summary(print_fn=write_summary)
summary_file.writelines(summary)
summary_file.close()

plot_model(model, show_shapes=True, show_layer_names=True, to_file=output_path+'/model.png')
open(output_path+'/model.json','w').write(json.dumps(model.to_json(indent = 4)))


# ============================ #
# ==== Fit Neural Network ==== #
# ============================ #

#if cont: model.load_weights(output_path+'/model.h5')

print('   * Training begins...')
try:
    model.fit(train,
              epochs=epoch,
              verbose=1,
              validation_data = val,
              workers = 4,
              # use_multiprocessing = True,
              validation_freq = 1,
              max_queue_size=queue,#max_queue_size,  # defaults to 10
              callbacks = [
                           ModelCheckpoint(output_path+'/'+to_run+'_model.h5', monitor='val_loss', 
                                           verbose=1, save_best_only=True),
                           EarlyStopping(verbose=1, patience=epoch//2, monitor='val_loss', 
                                         mode='min',min_delta=0.01),
                           ReduceLROnPlateau(monitor='val_loss', factor=0.5,
                                             patience=patience, verbose=1, 
                                             mode='min', min_delta=0.01, 
                                             cooldown=0, min_lr=1e-10),
                           TerminateOnNaN()
                           ]
              )
except KeyboardInterrupt:
    print('   * Training stopped by the user.')

train.Reset()
val.Reset()

# save model history
model_history = pd.DataFrame.from_dict(model.history.history)
model_history.to_csv(output_path+'/model_history.csv')

# ============================= #
# ==== Extract the results ==== #
# ============================= #


for key, item in model.history.history.items():
    if 'val' not in key:
        legend = []
        fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
        plt.plot(item)
        legend.append(key)
        if 'lr' not in key:
            plt.plot(model.history.history['val_'+key])
            legend.append('val_'+key)
        plt.yscale('log')
        plt.title('Training History')
        plt.ylabel(key)
        plt.xlabel('epoch')
        plt.legend(legend, loc='best')
        plt.savefig(output_path+'/'+key+'.png', bbox_inches = 'tight')
        plt.close()


# ================================= #
# ==== Test the Neural Network ==== #
# ================================= #


if to_run in ['covnet','bayescnn']:
    test = DataGenerator(test_dict,batch_size=50000,
                         std=normalize_meanstd)
elif 'lstm' in to_run or 'brnn' in to_run:
    test = DataGenerator(test_dict,batch_size=50000,
                         std=lstm_std)
elif 'enn' in to_run:
    test = DataGenerator(test_dict,batch_size=50000,
                         std=[normalize_meanstd,lstm_std])

X_test, y_test = test[0]
yhat = model.predict(X_test,verbose=True)
test.Reset()

fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
# sys1 = fig.add_subplot()
binss = np.linspace(0,1,15)
bkg = plt.hist(yhat[np.array(y_test) == 0],
         histtype='bar',  bins=binss, 
         label=r'Background',density=True,
         linewidth=0, color='tab:blue', linestyle='solid')
sig = plt.hist(yhat[np.array(y_test) == 1],
         histtype='step', bins=binss, 
         label=r'Signal',
         color=None, edgecolor='tab:red', linewidth=2, linestyle="dashed",
         bottom=None, cumulative=False, density=True, align="mid", orientation="vertical")
leg = plt.legend(loc='upper right',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
plt.xlabel('Classifier Output',fontsize=20)
plt.ylabel('Normalized Number of Events',fontsize=20)
plt.axis([0,1,1e-2,150])
plt.yscale("log", nonposy='clip')
plt.savefig(output_path+'/class_output.png', bbox_inches = 'tight')
plt.close('all')


## Plot variational ROC


fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')

roc_dict= {}
for ix in range(len(test)):
    X_test, y_test = test[ix]
    yhat = model.predict(X_test,verbose=True)

    fpr, tpr, tresholds = roc_curve(y_test, yhat,pos_label=1, 
                                    drop_intermediate=False)
    roc_auc = auc(fpr, tpr)
    scores = model.evaluate(X_test,y_test,verbose=0,batch_size=100,return_dict=True)
    for key,item in scores.items():
        roc_dict[key+'_'+str(ix)] = float(item)
    roc_dict['tpr_'+str(ix)] = tpr.tolist() if type(tpr)==np.ndarray else tpr
    roc_dict['fpr_'+str(ix)] = fpr.tolist() if type(fpr)==np.ndarray else fpr
    roc_dict['tresholds_'+str(ix)] = tresholds.tolist() if type(tresholds)==np.ndarray else tresholds
    roc_dict['auc'+str(ix)] = float(roc_auc)
    plt.plot(tpr,np.where(fpr>0,1./fpr,1./min([x for x in fpr if x>0])),
             lw=2, label=r'ROC (AUC = ${:.3f}$)'.format(roc_auc)) 

x = np.linspace(min([x for x in fpr if x>0]),1.,200)
plt.plot(x, 1./x, 'k--', lw=2)

plt.yscale('log')
plt.ylabel('Background Rejection ($1/\epsilon_B$)',fontsize=20)
plt.xlabel('True Positive Rate ($\epsilon_S$)',fontsize=20)
leg = plt.legend(loc='best',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
all_auc = np.array([item for key, item in roc_dict.items() if 'auc' in key])
plt.title(r"AUC = $ {:.4f} \pm {:.4f}$".format(all_auc.mean(), all_auc.std()/np.sqrt(float(len(all_auc))) ))
#plt.savefig(os.path.join(path,'roc_curve.png'),bbox_inches='tight')
plt.savefig(output_path+'/roc.png', bbox_inches = 'tight')
plt.close('all')
open(output_path+'/roc.json','w').write(json.dumps(roc_dict, indent = 4))


if any([(x in to_run) for x in ['covnet','enn','bayescnn','semibenn']]):
    if 'enn' in to_run : X_test = X_test[0]
    plot_correlations(get_error_correlations(X_test,yhat,y_test),
                      extent=[-1.6,1.6,-1.6,1.6],name=output_path+'/err_corr.png')

elif 'lstm' in to_run or 'brnn' in to_run:
    from EnsembleNN.Ensemble.DataGenerator import DataGenerator as DatGen
    test = DatGen(test_dict,batch_size=50000, shuffle=True, std=[normalize_meanstd,lstm_std])
    X_test, y_test = test[0]
    yhat = model.predict(X_test[1],verbose=True)
    plot_correlations(get_error_correlations(X_test[0],yhat,y_test),
                      extent=[-1.6,1.6,-1.6,1.6],name=output_path+'/err_corr.png')



