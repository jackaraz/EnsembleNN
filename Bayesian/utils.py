#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 22:41:20 2021

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy          as np
import matplotlib.pyplot as plt
from matplotlib.colors   import LogNorm, Normalize
plt.rcParams['xtick.direction']           = 'in'
plt.rcParams['ytick.direction']           = 'in'
plt.rcParams['xtick.top']                 = True
plt.rcParams['xtick.minor.visible']       = True
plt.rcParams['ytick.right']               = True
plt.rcParams['ytick.minor.visible']       = True
plt.rcParams['xtick.labelsize']           = 15
plt.rcParams['ytick.labelsize']           = 15
plt.rcParams['axes.labelsize']            = 20

from scipy.optimize import curve_fit

def plot_mu_sig(yhat,text=[]):
    aleatoric = (yhat * (1.-yhat)).mean(axis=1)
    epistemic = (yhat**2).mean(axis=1) - (yhat.mean(axis=1))**2

    f = lambda x,a,b,c : a*x**2 + b*x + c
    popt, _ = curve_fit(f, yhat.mean(axis=1),yhat.std(axis=1))
    a, b, c = popt
    
    fig  = plt.figure(figsize=(10, 7))
    sc   = plt.scatter(yhat.mean(axis=1),yhat.std(axis=1),c=aleatoric+epistemic,
                       s=5,cmap='jet',vmin=0,vmax=(aleatoric+epistemic).max())
    cbar = plt.colorbar(sc)
    cbar.set_label('Total Uncertainty',fontsize=20)
    x_line = np.arange(0, 1, .01)
    y_line = f(x_line, a, b,c )
    plt.plot(x_line,y_line,color='k',lw=2,linestyle='dashed')
    plt.xlim([0.-.01,1.01])
    plt.ylim([0.,yhat.std(axis=1).max()])
    plt.xlabel('$\hat{\mu}_{bayes}$')
    plt.ylabel('$\hat{\sigma}_{bayes}$')
    if text != []:
        plt.text(text[0],text[1],text[2],color='darkred',fontsize=15)
    plt.show()

def plot_epistemic_aleatoric(yhat,text=[]):
    aleatoric = (yhat * (1.-yhat)).mean(axis=1)
    epistemic = (yhat**2).mean(axis=1) - (yhat.mean(axis=1))**2
    
    f = lambda x,a,b,c : a*x**2 + b*x + c
    popt, _ = curve_fit(f, yhat.mean(axis=1),epistemic)
    a, b, c = popt
    
    fig  = plt.figure(figsize=(10, 7))
    sc = plt.scatter(yhat.mean(axis=1),epistemic,c=aleatoric,s=5,
                     cmap='jet',vmin=0,vmax=aleatoric.max())
    cbar = plt.colorbar(sc)
    cbar.set_label('Aleatoric Uncertainty',fontsize=20)
    
    x_line = np.arange(0, 1, .01)
    y_line = f(x_line, a, b,c )
    plt.plot(x_line,y_line,color='k',lw=2,linestyle='dashed')
    
    plt.xlim([0.-0.01,1.01])
    plt.ylim([1e-8,epistemic.max()])
    plt.xlabel('$\hat{\mu}_{bayes}$')
    plt.ylabel('Epistemic Uncertainty')
    plt.yscale('log')
    if text != []:
        plt.text(text[0],epistemic.max()+1e-3,text[2],color='darkred',fontsize=15)
    plt.show()


def plot_compare_std(rnn,cnn,mean,enn):
    f = lambda x,a,b,c : a*x**2 + b*x + c

    fig  = plt.figure(figsize=(9, 7))
    
    for yhat,color,label in [(rnn,'tab:blue','RNN'),
                             (cnn,'tab:green','CNN'),
                             (mean,'tab:orange','Mean'),
                             (enn,'tab:red','ENN')]:
 
        popt, pcov = curve_fit(f, yhat.mean(axis=1),yhat.std(axis=1))
        perr = np.sqrt(np.diag(pcov))

        err_up = popt+2.*perr
        err_dn = popt-2.*perr
        x_line = np.arange(0., 1., .01)
        plt.plot(x_line,f(x_line, *popt),color=color,lw=2,linestyle='solid',label=label)
        plt.fill_between(x_line,f(x_line, *err_up),f(x_line, *err_dn),color=color,alpha=0.1,lw=0)
    
    leg = plt.legend(loc='upper right',fontsize=15)
    leg.get_frame().set_alpha(0.5)
    leg.get_frame().set_linewidth(0.0)
    leg.set_zorder(100)
    plt.xlim([0,1])
    plt.ylim([0,0.06])
    plt.xlabel('$\hat{\mu}_{bayes}$')
    plt.ylabel('$\hat{\sigma}_{bayes}$')
    plt.show()

def plot_compare_epistemic(rnn,cnn,mean,enn):
    f = lambda x,a,b,c : a*x**2 + b*x + c

    fig  = plt.figure(figsize=(9, 7))
    
    for yhat,color,label in [(rnn,'tab:blue','RNN'),
                             (cnn,'tab:green','CNN'),
                             (mean,'tab:orange','Mean'),
                             (enn,'tab:red','ENN')]:
        
        epistemic = (yhat**2).mean(axis=1) - (yhat.mean(axis=1))**2
        popt, pcov = curve_fit(f, yhat.mean(axis=1),epistemic)
        perr = np.sqrt(np.diag(pcov))

        err_up = popt+2.*perr
        err_dn = popt-2.*perr
        x_line = np.arange(0., 1., .01)
        plt.plot(x_line,f(x_line, *popt),color=color,lw=2,linestyle='solid',label=label)
        plt.fill_between(x_line,f(x_line, *err_up),f(x_line, *err_dn),color=color,alpha=0.1,lw=0)
    
    leg = plt.legend(loc='upper right',fontsize=15)
    leg.get_frame().set_alpha(0.5)
    leg.get_frame().set_linewidth(0.0)
    leg.set_zorder(100)
    plt.xlim([0,1])
    plt.ylim([0,.004])
    plt.xlabel('$\hat{\mu}_{bayes}$')
    plt.ylabel('Epistemic Uncertainty')
    plt.show()


def plot_compare_aleatoric(rnn,cnn,mean,enn):
    f = lambda x,a,b,c : a*x**2 + b*x + c

    fig  = plt.figure(figsize=(9, 7))
    
    for yhat,color,label in [(rnn,'tab:blue','RNN'),
                             (cnn,'tab:green','CNN'),
                             (mean,'tab:orange','Mean'),
                             (enn,'tab:red','ENN')]:
        
        aleatoric = (yhat * (1.-yhat)).mean(axis=1)
        popt, pcov = curve_fit(f, yhat.mean(axis=1),aleatoric)
        perr = np.sqrt(np.diag(pcov))

        err_up = popt+2.*perr
        err_dn = popt-2.*perr
        x_line = np.arange(0., 1., .01)
        plt.plot(x_line,f(x_line, *popt),color=color,lw=2,linestyle='solid',label=label)
        plt.fill_between(x_line,f(x_line, *err_up),f(x_line, *err_dn),color=color,alpha=0.1,lw=0)


    leg = plt.legend(loc='upper right',fontsize=15)
    leg.get_frame().set_alpha(0.5)
    leg.get_frame().set_linewidth(0.0)
    leg.set_zorder(100)
    plt.xlim([0.4,.6])
    plt.ylim([0.24,.26])
    plt.xlabel('$\hat{\mu}_{bayes}$')
    plt.ylabel('Aleatoric Uncertainty')
    plt.show()