# Combine and Conquer: Event Reconstruction with Bayesian Ensemble Neural Networks

[![JHEP](https://img.shields.io/static/v1?style=plastic&label=DOI&message=JHEP04(2021)296&color=blue)](https://doi.org/10.1007/JHEP04(2021)296) [![arxiv](https://img.shields.io/static/v1?style=plastic&label=arXiv&message=2102.01078&color=brightgreen)](https://arxiv.org/abs/2102.01078)

  Ensemble Neural Network analysis for the paper [arXiv:2102.01078](https://arxiv.org/abs/2102.01078)

### Abstract

Ensemble learning is a technique where multiple component learners are combined through a protocol. We propose an Ensemble Neural Network (ENN) that uses the combined latent-feature space of multiple neural network classifiers to improve the representation of the network hypothesis. We apply this approach to construct an ENN from Convolutional and Recurrent Neural Networks to discriminate top-quark jets from QCD jets. Such ENN provides the flexibility to improve the classification beyond simple prediction combining methods by increasing the classification error correlations. In combination with Bayesian techniques, we show that it can reduce epistemic uncertainties and the entropy of the hypothesis by exploiting various kinematic correlations of the system.

### File structure
- `EnsembleNN/Convolutional`: utilities for convolutional network training. `DataGenerator` is design to feed image data from various compressed `numpy` files. `utils` has the analysis tools for images such as correlation plots.
- `EnsembleNN/Recurrent`: utilities for recurrent network training. Same as before.
- `EnsembleNN/Ensemble`: utilities for ensemble network training. Particularly for parallel combining method.
- `EnsembleNN/Bayesian`: utilities for plotting Bayesian Network outputs.
- `EnsembleNN/examples`: working examples used for the paper.
- `EnsembleNN/experimental`: various tests to ensure correct network training and prediction interpretation.
- `EnsembleNN/preprocessing `
  - `RNN_preprocessing `: utilities for recurrent network preprocessing.
  - `CNN_preprocessing `: utilities for convolutional network preprocessing.

Network architectures used in the paper:
- RNN: `EnsembleNN/Models/LSTMArchi.py`
- CNN: `EnsembleNN/Models/DeepTopXS.py`
- ENN: `EnsembleNN/Models/EnsembleNN.py`
- Bayesian versions:
  - RNN: `EnsembleNN/Models/simpleBRNN.py`
  - CNN: `EnsembleNN/Models/simpleBCNN.py`
  - ENN single-layer: `EnsembleNN/Models/simpleBENN_singlelayer.py`
  - ENN two-layers: `EnsembleNN/Models/simpleBENN.py`

Main example for running the networks: `EnsembleNN/examples/simpleRun.py`
To see the input options type:
```bash
$ python simpleRun.py -h
```

Main example for event preprocessing is `EnsembleNN/examples/CreateENNData.py`. Depending on the given file paths, it generates CNN and RNN type of data divided into files with 5K events each. Files are saved as compressed `numpy` files. For more information about the data [see this link](https://zenodo.org/record/2603256#.Xjnnty_MzRZ).

Most of the plots for the paper are prepared in `EnsembleNN/examples/OutputAnalysis.ipynb` and `EnsembleNN/examples/comparison.ipynb`. Please see the [paper](https://arxiv.org/abs/2102.01078) for the latest versions of the plots.

## Citation

```latex
@article{Araz:2021wqm,
    author = "Araz, Jack Y. and Spannowsky, Michael",
    title = "{Combine and Conquer: Event Reconstruction with Bayesian Ensemble Neural Networks}",
    eprint = "2102.01078",
    archivePrefix = "arXiv",
    primaryClass = "hep-ph",
    reportNumber = "IPPP/20/74",
    doi = "10.1007/JHEP04(2021)296",
    journal = "JHEP",
    volume = "04",
    pages = "296",
    year = "2021"
}
```
