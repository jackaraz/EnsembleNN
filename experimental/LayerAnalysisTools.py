#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 09:08:55 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np
import matplotlib.pyplot as plt
import pylab as pl
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable

def get_output_layer_avg(x_test,func,shape):
    avg_conv=np.zeros(shape)
    # if layer==1:
    #   avg_conv=np.zeros((32,17,17))
    # elif layer==2:
    #   avg_conv=np.zeros((64,7,7))
    # elif layer==3:
    #   avg_conv=np.zeros((64,3,3))
    start = 0
    for i_layer in range(1,len(x_test)+1):
        Conv = func([0] + [np.array(x_test[start:i_layer])])
        print('Conv_array type  = ',type(Conv))
        Conv=np.asarray(Conv)
        Conv = np.squeeze(Conv) #Remove single-dimensional entries from the shape of an array.
        Conv=np.swapaxes(Conv,0,2) #Interchange two axes of an array.
        Conv=np.swapaxes(Conv,1,2)
        avg_conv=avg_conv+Conv
        start = i_layer

    print("avg image shape after adding all images = ", np.shape(avg_conv)) #create an array of zeros for the image
    return avg_conv

def nice_imshow(ax, data, vmin=None, vmax=None, cmap=None):
    """Wrapper around pl.imshow"""
    if cmap is None:
        cmap = cm.jet
    if vmin is None:
        vmin = data.min()
    if vmax is None:
        vmax = data.max()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    im = ax.imshow(data, vmin=vmin, vmax=vmax, interpolation='nearest', cmap=cmap)
    pl.colorbar(im, cax=cax)


def make_mosaic(imgs, nrows, ncols, border=1):
    """
    Given a set of images with all the same shape, makes a
    mosaic with nrows and ncols
    """
    nimgs = imgs.shape[0]
    imshape = imgs.shape[1:]
    
    mosaic = np.ma.masked_all((nrows * imshape[0] + (nrows - 1) * border,
                            ncols * imshape[1] + (ncols - 1) * border),
                            dtype=np.float32)
    
    paddedh = imshape[0] + border
    paddedw = imshape[1] + border
    for i in range(nimgs):
        row = int(np.floor(i / ncols))
        col = i % ncols
        
        mosaic[row * paddedh:row * paddedh + imshape[0],
               col * paddedw:col * paddedw + imshape[1]] = imgs[i]
    return mosaic

