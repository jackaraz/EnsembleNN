#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 18:06:24 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

from TopTagger.preprocessing.image_preprocessing.utils import *
# from TopTagger.preprocessing.image_preprocessing.image_preprocess import output
from TopTagger.preprocessing.image_preprocessing.jet_image_plotting import *
import numpy as np
import os

def output_image_array_data_true_value(Image,type,name,path):
    Nimages=len(Image)
    array_name=str(name)+'_'+str(Nimages)+'_'+type
    np.savez_compressed(os.path.join(path,array_name+'.npz'),image=Image, 
                        shape=np.shape(Image))

def plot(image_QCD,image_top,name,extent=[-1.6,1.6,-1.6,1.6]):
    fig, axarr = plt.subplots(1, 2, sharey=True)
    fig.set_size_inches(12, 6)
    axarr[0].imshow(np.array(image_QCD).mean(axis=0),'gnuplot',extent=extent)
    axarr[1].imshow(np.array(image_top).mean(axis=0),'gnuplot',extent=extent)  
    axarr[0].set_title('QCD')
    axarr[1].set_title(r'$t\bar{t}$')
    plt.tight_layout()
    plt.savefig('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/update_'+name+'.png', bbox_inches = 'tight')

def get_lims(nevt,maxevt):
    if maxevt<nevt:
        return [maxevt]
    else:
        return np.linspace(nevt,maxevt,int(maxevt/nevt),dtype=int)


nevt = 10000

save_path = '/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/'

for mode in ['train','val','test']:
    print('Running for '+mode)
    jets_sig,subjets_sig, Njets_sig=loadfiles('collections/sig/'+mode, mass_dom=[0.,1e4])
    jets_bg,subjets_bg, Njets_bg=loadfiles('collections/bkg/'+mode, mass_dom=[0.,1e4])
    maxevt = min([Njets_sig,Njets_bg])
    
    lower = 0
    iteration = 1
    lims = get_lims(nevt,maxevt)
    print('   * lims : ',lims)
    for upper in lims:
        print('   * lower {:.0f} upper {:.0f}'.format(lower,upper))
        if lower >= upper:
            continue
        subjets_sig_current = np.array([subjets_sig[0][lower:upper],
                                        subjets_sig[1][lower:upper],
                                        subjets_sig[2][lower:upper]])

        subjets_bg_current  = np.array([subjets_bg[0][lower:upper],
                                        subjets_bg[1][lower:upper],
                                        subjets_bg[2][lower:upper]])

        print('   * Jet size :',subjets_sig_current.shape,subjets_bg_current.shape)
        images_sig=preprocess(subjets_sig_current)
        images_bg=preprocess(subjets_bg_current)  
        myN_jets=np.min([len(images_sig),len(images_bg),nevt]) 
        print("   * Number of images (sig=bg) used for analysis",myN_jets)
        im_pt = []
        for im in images_sig:
            im_pt.append(np.array(im).sum())
        print("   * Min and Max PT of the signal images: {:.2f} GeV, {:.2f} GeV".format(min(im_pt),max(im_pt)))

        images_sig=normalize(images_sig[0:myN_jets],method=1000.)
        images_bg=normalize(images_bg[0:myN_jets],method=1000.)
        im_pt = []
        for im in images_sig:
            im_pt.append(np.array(im).sum())
        print("   * Min and Max PT of the normalized signal images: {:.2f} TeV, {:.2f} TeV".format(min(im_pt),max(im_pt)))

        sig_image_norm = images_sig#standardize_images(np.array(images_sig),np.array(images_sig+images_bg),'std')
        bg_image_norm  = images_bg#standardize_images(np.array(images_bg),np.array(images_sig+images_bg),'std')
        
        output_image_array_data_true_value(sig_image_norm,
                                           'norm','tt_'+mode+'_{:03d}'.format(iteration),
                                           save_path+mode+'/')
        output_image_array_data_true_value(bg_image_norm,
                                           'norm','QCD_'+mode+'_{:03d}'.format(iteration),
                                           save_path+mode+'/')
        plot(bg_image_norm,sig_image_norm,mode+'_{:03d}_N{:.0f}'.format(iteration,myN_jets))
        lower=upper
        iteration+=1
