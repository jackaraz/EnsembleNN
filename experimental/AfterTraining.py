#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 13:06:01 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from keras.models import model_from_json
import numpy as np
from TopTagger.CovNet.CovNet_tools import DataGenerator
from TopTagger.IOinterface.AnalysisArchitecture import Analysis as archi
import glob, os, json

Analysis = archi('ANALYSIS_1', main_path='/mt/batch/jaraz/RNN_CNN')

def get_total_size(array_list):
    total_images=0
    for fl in array_list:
        total_images += np.load(fl)['shape'][0]
    return total_images

test_signal_array_list     = glob.glob(Analysis.AnalysisPath+'/images2/test/tt_*')
test_background_array_list = glob.glob(Analysis.AnalysisPath+'/images2/test/QCD_*')


with open(os.path.join(Analysis.Model,'model_0001.json'), 'r') as f:
    loaded_model_json = json.load(f)
model = model_from_json(loaded_model_json)
model.load_weights(os.path.join(Analysis.Model,'model_0001.h5'))

test_x_test_y = DataGenerator(test_signal_array_list, 
                              test_background_array_list, 
                              0.0,0.0,1.0, 'test',
                              batch_size = 128, 
                              shuffle=False)

test_step = int(2*get_total_size(test_signal_array_list)/128)


# yhat       = model.predict(X_test, verbose=1)
yhat = model.predict_generator(test_x_test_y.generate(),
                                steps=test_step,
                                max_queue_size=test_step/5,
                                use_multiprocessing=False,
                                verbose=1)

y_test = np.array(test_x_test_y.y_test[:yhat.shape[0]])
print('yhat shape ', yhat.shape, 'y test shape', y_test.shape)
yhat_cls   = np.argmax(yhat, axis=1)
Analysis.save_classifier_output(yhat,y_test)
from sklearn.metrics import classification_report
# print('\n\n')
print(classification_report(y_test, yhat_cls, labels=[0,1],target_names=['Signal','Background']))
