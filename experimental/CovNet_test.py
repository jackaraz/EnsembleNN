#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 22:27:22 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""
import sys, os
# sys.path.append('/home/jack/packages/python/RNN_CNN')
from TopTagger.CovNet.ConvolutionalNN import fit_generator
from TopTagger.CovNet.DataGenerator import DataGenerator
from TopTagger.IOinterface.AnalysisArchitecture import Analysis
from keras import optimizers
from keras.callbacks import ModelCheckpoint,EarlyStopping,ReduceLROnPlateau
from keras.layers import Dense, Dropout, Conv2D, Flatten, LeakyReLU

# import tensorflow as tf
# print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
# print("Is GPU available:   ", tf.test.is_gpu_available())
# tf.debugging.set_log_device_placement(True)


# gpus = tf.config.experimental.list_physical_devices('GPU')
# if gpus:
#     try:
#         tf.config.experimental.set_visible_devices(gpus[0], 'GPU')
#         tf.config.experimental.set_memory_growth(gpus[0], True)
#         logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#         print("=====>", len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
#     except RuntimeError as e:
#         # Memory growth must be set before GPUs have been initialized
#         print(e)

#[x.split('.npz')[0].split('_')[2] for x in  os.listdir('.') if x.startswith('QCD') ]

Analysis = Analysis('ANALYSIS_1', main_path='/mt/batch/jaraz/RNN_CNN')
#Analysis.GetNewModel()

training_generator   = DataGenerator(Analysis.std_train,
                                     batch_size=128, dim=(40,40),
                                     shuffle=True)

validation_generator = DataGenerator(Analysis.std_val,
                                     batch_size=128, dim=(40,40),
                                     shuffle=True)

test_generator = DataGenerator(Analysis.std_test,
                               batch_size=128, dim=(40,40),
                               shuffle=True, truth=True)

# base_model = {
#                  1  : Conv2D(32, kernel_size=(7,7), strides=1),
#                  2  : LeakyReLU(),
#                  3  : Dropout(0.2),
#                  4  : Conv2D(64, kernel_size=(7,7), strides=1),
#                  5  : LeakyReLU(),
#                  6  : Dropout(0.2),
#                  7  : Conv2D(128, kernel_size=(5,5), strides=1),
#                  8  : LeakyReLU(),
#                  9  : Dropout(0.2),
#                  10 : Conv2D(256, kernel_size=(5,5), strides=1),
#                  11 : LeakyReLU(),
#                  12 : Flatten(),
#                  13 : Dropout(0.2),
#                  14 : Dense(1, activation='sigmoid')
#              }
#X_train = np.expand_dims(X_train, -1)
# define model
from TopTagger.Models.DeepTop import DeepTop as model
# model   = define_model((40, 40, 1), model_dict=base_model)
model.summary()

learning_rate = 1e-5
adam = optimizers.Adam(learning_rate=learning_rate)
model.compile(optimizer=adam, loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

#model = fit_generator(model, training_generator, validation_generator, 
#                      verbose=True, epoch=10)
#X_train, y_train = training_generator[0]
#X_val, y_val     = validation_generator[0]

model = fit_generator(model, training_generator, validation_generator, 
                      verbose=True, epoch=5,workers=10,
                      callbacks = [ModelCheckpoint(Analysis.GetWeightName(), 
                                                   monitor='val_loss', 
                                                   verbose=True, 
                                                   save_best_only=True),
                                   ReduceLROnPlateau(monitor='val_loss', 
                                                     factor=0.5, patience=10,
                                                     verbose=1, mode='auto',
                                                     min_delta=0.01, 
                                                     cooldown=0, 
                                                     min_lr=5e-10),
                                   EarlyStopping(verbose=True, 
                                                 patience=20, 
                                                 monitor='val_loss')]
                                   
                                   )

Analysis.save_model(model)

yhat = model.predict_generator(test_generator,steps=50, verbose=1)
