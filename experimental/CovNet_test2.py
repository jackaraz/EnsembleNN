#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 15:37:17 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

from TopTagger.Models.DeepTop import DeepTop as model
from TopTagger.CovNet.utils import *
from TopTagger.IOinterface.AnalysisArchitecture import Analysis as archi
import glob
# from keras.models import Sequential
# from keras.layers import Dense, Dropout, Flatten
# from keras.layers import Conv2D, MaxPooling2D
# from keras.layers.normalization import BatchNormalization
# from keras import regularizers
# from keras import backend as K # We are using TensorFlow as Keras backend
# from keras import optimizers
# from keras.utils import np_utils

# K.tensorflow_backend._get_available_gpus()
# import tensorflow as tf
# tf.test.is_gpu_available(
#     cuda_only=False, min_cuda_compute_capability=None
# )
# from tensorflow.compat.v1 import ConfigProto, InteractiveSession
import tensorflow as tf
# config = ConfigProto()
# config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.6
# sess = InteractiveSession(config=config)
# from tensorflow.keras.backend.tensorflow_backend import set_session
# from tensorflow.python.client import device_lib
# print(device_lib.list_local_devices())
# print("GPU available : ",tf.test.is_gpu_available())
# config = tf.compat.v1.ConfigProto()
# config.gpu_options.per_process_gpu_memory_fraction = 0.45
# config.gpu_options.visible_device_list = "0"
# set_session(tf.compat.v1.Session(config=config))

# tf.random.set_seed(40)
Analysis = archi('ANALYSIS_1', main_path='/mt/batch/jaraz/RNN_CNN',model_num=1)


def get_total_size(array_list):
    total_images=0
    for fl in array_list:
        total_images+= np.load(fl)['shape'][0]
    return total_images


train_signal_array_list     = glob.glob('/mt/batch/jaraz/CovNet_Images/train/tt_*')
train_background_array_list = glob.glob('/mt/batch/jaraz/CovNet_Images/train/QCD_*')
# print('-----------'*10)
# print('Train Signal List : '+', '.join(train_signal_array_list))
# print('-----------'*10)
# print('Train BKG List :'+', '.join(train_background_array_list))
# print('-----------'*10)

val_signal_array_list     = glob.glob('/mt/batch/jaraz/CovNet_Images/val/tt_*')
val_background_array_list = glob.glob('/mt/batch/jaraz/CovNet_Images/val/QCD_*')


# If the fraction from my input files for tratining is different from 
# (train_frac, val_frac, test_frac)=(1,0,0), then also multiply Ntrain*train_frac
Ntrain=2*get_total_size(train_signal_array_list)*sample_relative_size
print('Ntrain',Ntrain)


# input_shape = (37, 37, 1)
# model = Sequential()
# model.add(Conv2D(32, (7,7), activation='relu',padding='same',
#                    input_shape=input_shape))
# model.add(LeakyReLU())
# model.add(Dense(64, activation='relu'))
# model.add(Dense(2, activation='softmax'))
# from keras.layers import Dense, Input, Flatten, Conv2D, MaxPooling2D
# from keras.models import Model
# inputs = Input(shape=(37,37,1))
# first_layer = Conv2D(8, (4, 4), activation='relu',padding='same')(inputs)
# hidden = Conv2D(8, (4, 4), activation='relu',padding='same')(first_layer)
# hidden = MaxPooling2D(pool_size=(2, 2))(hidden)
# hidden = Conv2D(8, (4,4), activation='relu',padding='same')(hidden)
# hidden = Conv2D(8, (4,4), activation='relu',padding='same')(hidden)
# hidden = Flatten()(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# outputs = Dense(2, activation='softmax')(hidden)
# model = Model(inputs,outputs)
model.summary()


#1) Adadelta
# Adadelta=keras.optimizers.Adadelta(lr=learning_rate[0], rho=0.95, epsilon=1e-08, decay=0.0)

#2) Adam
from tensorflow.keras.optimizers import Adam
Adam= Adam(lr=1e-3)#, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

#3) Sigmoid gradient descent: the convergence is much slower than with Adadelta
# sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=Adam,
              metrics=['accuracy'])

my_batch_size = 200
# tested batch size 128 and 200 -> no difference

val_steps_per_epoch = int(2*get_total_size(val_signal_array_list)*sample_relative_size/my_batch_size)


train_x_train_y = DataGenerator(train_signal_array_list, 
                                train_background_array_list, 
                                1.0,0.0,0.0, 'train',
                                batch_size = my_batch_size)

val_x_val_y = DataGenerator(val_signal_array_list, 
                            val_background_array_list, 
                            0.0,1.0,0.0, 'val',
                            batch_size = my_batch_size)


save_weights = tf.keras.callbacks.ModelCheckpoint(Analysis.GetWeightName(), 
                                                monitor='val_loss', 
                                                verbose=True, 
                                                save_best_only=True)

lR_decay = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, 
                                             patience=15, verbose=1, mode='auto',
                                             min_delta=0.01, cooldown=0, min_lr=1e-5)

early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.001, 
                                           patience=50, verbose=1, mode='auto')


print('total_images =',2*get_total_size(train_signal_array_list))  

my_steps_per_epoch= (get_total_size(train_signal_array_list)+\
                     get_total_size(train_background_array_list))//my_batch_size

print('my_steps_per_epoch =',my_steps_per_epoch)
my_max_q_size=my_steps_per_epoch/6


model.fit_generator(generator = train_x_train_y.generate(),
                    steps_per_epoch = my_steps_per_epoch, #This is the number of files that we use to train in each epoch
                    epochs=5,
                    verbose=1,
                    validation_data = val_x_val_y.generate(),
                    validation_steps = (get_total_size(val_signal_array_list)+\
                                        get_total_size(val_background_array_list))//my_batch_size,
                    max_queue_size=my_max_q_size,  # defaults to 10
                    callbacks=[lR_decay,save_weights]
                   )
# steps_per_epoch: Integer.
#     Total number of steps (batches of samples)
#     to yield from `generator` before declaring one epoch
#     finished and starting the next epoch. It should typically
#     be equal to the number of samples of your dataset
#     divided by the batch size.
#     Optional for `Sequence`: if unspecified, will use
#     the `len(generator)` as a number of steps.
# validation_steps: Only relevant if `validation_data`
#     is a generator. Total number of steps (batches of samples)
#     to yield from `validation_data` generator before stopping
#     at the end of every epoch. It should typically
#     be equal to the number of samples of your
#     validation dataset divided by the batch size.
#     Optional for `Sequence`: if unspecified, will use
#     the `len(validation_data)` as a number of steps.
# max_queue_size: Integer. Maximum size for the generator queue.
#     If unspecified, `max_queue_size` will default to 10.
# workers: Integer. Maximum number of processes to spin up
#     when using process-based threading.
#     If unspecified, `workers` will default to 1. If 0, will
#     execute the generator on the main thread.
# use_multiprocessing: Boolean.
#     If `True`, use process-based threading.
#     If unspecified, `use_multiprocessing` will default to `False`.
#     Note that because this implementation
#     relies on multiprocessing,
#     you should not pass non-picklable arguments to the generator
#     as they can't be passed easily to children processes.
# shuffle: Boolean. Whether to shuffle the order of the batches at
#     the beginning of each epoch. Only used with instances
#     of `Sequence` (`keras.utils.Sequence`).
#     Has no effect when `steps_per_epoch` is not `None`.
# initial_epoch: Integer.
#     Epoch at which to start training
#     (useful for resuming a previous training run).

Analysis.save_model(model)
# Create the test set and evaluate the model

test_signal_array_list     = glob.glob('/mt/batch/jaraz/CovNet_Images/test/tt_*')
test_background_array_list = glob.glob('/mt/batch/jaraz/CovNet_Images/test/QCD_*')


# X_test, y_test = generate_input_sets(test_signal_array_list, test_background_array_list, 0.0,0.0,1.0, 'test')
# score = model.evaluate(X_test, y_test, verbose=1)
# print('Test loss = ', score[0])
# print('Test accuracy = ', score[1])

# print('Test signal files : '+', '.join(test_signal_array_list))
# print('Test bkg files    : '+', '.join(test_background_array_list))

batch_size = 128

test_x_test_y = DataGenerator(test_signal_array_list, 
                              test_background_array_list, 
                              0.0,0.0,1.0, 'test',
                              batch_size = batch_size, 
                              shuffle=False)

test_step = (get_total_size(test_signal_array_list)+\
             get_total_size(test_background_array_list))//batch_size


# yhat       = model.predict(X_test, verbose=1)
yhat = model.predict_generator(test_x_test_y.generate(),
                                steps=test_step,
                                max_queue_size=test_step/5,
                                use_multiprocessing=False,
                                verbose=1)
# y_test = []
# for sig_file in range(len(test_signal_array_list)):
#     y_test += [0]*get_total_size([test_signal_array_list[sig_file]])
#     y_test += [1]*get_total_size([test_background_array_list[sig_file]])
# y_test = np.array(y_test)
y_test = np.array(test_x_test_y.y_test[:yhat.shape[0]])
print('yhat shape ', yhat.shape, 'y test shape', y_test.shape)

yhat_cls   = np.argmax(yhat, axis=1)
# y_test_cls = np.argmax(y_test, axis=1)
Analysis.save_classifier_output(yhat,y_test.reshape(y_test.size))
from sklearn.metrics import classification_report
# print('\n\n')
print(classification_report(y_test, yhat_cls, labels=[0,1],target_names=['Signal','Background']))




# layer_outputs = [layer.output for layer in model.layers[:3]]
# from keras import models
# activation_model = models.Model(inputs=model.input, outputs=layer_outputs)

# activations = activation_model.predict(X_test)
# first_layer_activation = activations[0]













