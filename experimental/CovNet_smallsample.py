#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 15:37:17 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

from TopTagger.Models.DeepTop import DeepTop as model
from TopTagger.CovNet.utils import *
from TopTagger.IOinterface.AnalysisArchitecture import Analysis as archi
import keras as keras
from keras.models import Model

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
print("GPU available : ",tf.test.is_gpu_available())
config = tf.compat.v1.ConfigProto()
# config.gpu_options.per_process_gpu_memory_fraction = 0.45
# config.gpu_options.visible_device_list = "0"
set_session(tf.compat.v1.Session(config=config))

# tf.random.set_seed(40)
Analysis = archi('ANALYSIS_1', main_path='/mt/batch/jaraz/RNN_CNN',model_num=1)


train_signal_array_list     = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/train/tt_*')
train_background_array_list = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/train/QCD_*')

print(train_signal_array_list[0])

val_signal_array_list     = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/val/tt_*')
val_background_array_list = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/val/QCD_*')

# from keras.layers import Dense, Input, Flatten, Conv2D, MaxPooling2D


# inputs = Input(shape=(37,37,1))
# first_layer = Conv2D(8, (4, 4), activation='relu',padding='same')(inputs)
# hidden = Conv2D(8, (4, 4), activation='relu',padding='same')(first_layer)
# hidden = MaxPooling2D(pool_size=(2, 2))(hidden)
# hidden = Conv2D(8, (4,4), activation='relu',padding='same')(hidden)
# hidden = Conv2D(8, (4,4), activation='relu',padding='same')(hidden)
# hidden = Flatten()(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# hidden = Dense(64, activation='relu')(hidden)
# outputs = Dense(2, activation='softmax')(hidden)
# model = Model(inputs,outputs)



#1) Adadelta
# Adadelta=keras.optimizers.Adadelta(lr=learning_rate[0], rho=0.95, epsilon=1e-08, decay=0.0)

#2) Adam
Adam=keras.optimizers.Adam(lr=1e-5)#, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

#3) Sigmoid gradient descent: the convergence is much slower than with Adadelta
# sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)


my_batch_size = 200
# tested batch size 128 and 200 -> no difference

val_steps_per_epoch = int(2*10000/my_batch_size)


# X_train, y_train = np.random.random(size=(20000,37,37,1)), np.random.randint(0,high=2,size=(20000))
X_train, y_train = generate_input_sets(train_signal_array_list[0], train_background_array_list[0],
                                        1, 0,0, 'train')

# X_val, y_val = np.random.random(size=(20000,37,37,1)),np.random.randint(0,high=2,size=(20000))

X_val, y_val = generate_input_sets(val_signal_array_list[0], val_background_array_list[0],
                                        0, 1,0, 'val')


save_weights = keras.callbacks.ModelCheckpoint(Analysis.GetWeightName(), 
                                                monitor='val_loss', 
                                                verbose=True, 
                                                save_best_only=True)

lR_decay = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, 
                                             patience=15, verbose=1, mode='auto',
                                             min_delta=0.01, cooldown=0, min_lr=1e-5)

early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.001, 
                                            patience=50, verbose=1, mode='auto')



def fit(model,X_train, y_train, x_val,y_val,f=None, verbose=True, epoch=50):
    y = y_train
    if f == 'ballanced':
        f_sig = round(1./float(len(np.unique(y))),8)
        f_bkg = f_sig
    else:
        f_sig = f
        f_bkg = round((1.-f_sig)/float(len(np.unique(y))-1),8)
    cls_w = { # rebalance class representation
        0 : f_sig * (float(len(y)) / (y == 0).sum()),
        1 : f_bkg * (float(len(y)) / (y == 1).sum()),
            }
#    cls_w[0] = cls_w[0]*10.
    print('Training:')
    try:
        model.fit(
            X_train, y_train, #sample_weight=w_train,
            class_weight=cls_w,
            callbacks = [
                save_weights,
                # tensorboard_v1.TensorBoard(log_dir='/home/jack/packages/python/VBF_EFT/root_samples/leptonic/log', 
                #                            histogram_freq=0, batch_size=100, write_graph=True, write_grads=True,
                #                            write_images=True, embeddings_freq=0, embeddings_layer_names=None,
                #                            embeddings_metadata=None, embeddings_data=None, update_freq='epoch'),
                lR_decay,
                # early_stop
            ],
            epochs=epoch, 
            validation_data=(x_val,y_val),
            verbose=verbose,
            batch_size = 500,
            use_multiprocessing=True
    ) 
    except KeyboardInterrupt:
        print('Training ended early.')
    return model

model.summary()
model.compile(optimizer=keras.optimizers.Adam(lr=1e-3), loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model = fit(model,X_train, y_train, X_val, y_val,f='ballanced', 
                verbose=True, epoch=200)

Analysis.save_model(model)
# Create the test set and evaluate the model

test_signal_array_list     = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/test/tt_*')
test_background_array_list = glob.glob('/mt/batch/jaraz/RNN_CNN/ANALYSIS_1/images2/test/QCD_*')

X_test, y_test = generate_input_sets(test_signal_array_list[0], test_background_array_list[0],
                                       0, 0,1, 'test')

yhat = model.predict(X_test, verbose = True)
yhat_cls = np.argmax(yhat, axis=1)

# y_test_cls = np.argmax(y_test, axis=1)
Analysis.save_classifier_output(yhat,y_test)
from sklearn.metrics import classification_report
# print('\n\n')
print(classification_report(y_test, yhat_cls, labels=[0,1],target_names=['Signal','Background']))




# layer_outputs = [layer.output for layer in model.layers[:3]]
# from keras import models
# activation_model = models.Model(inputs=model.input, outputs=layer_outputs)

# activations = activation_model.predict(X_test)
# first_layer_activation = activations[0]













