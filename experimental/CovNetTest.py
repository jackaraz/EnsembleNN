#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 14:56:46 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np
import pandas as pd

# from tensorflow.keras.models import Model
# from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Input, Dense
from tensorflow.keras.metrics import AUC, Precision, KLDivergence
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, ModelCheckpoint
from tensorflow.keras.utils import plot_model
from tensorflow.keras import optimizers

# from mlxtend.evaluate import bias_variance_decomp
import matplotlib.pyplot as plt

from TopTagger.CovNet.DataGenerator import DataGenerator 
from TopTagger.CovNet.utils import plot_correlations, get_correlations
from TopTagger.Models.DeepTopXL import DeepTopXL as model

from glob import glob as glb
import os, json


# ================== #
# ==== NN Model ==== #
# ================== #

adam = optimizers.Adam(learning_rate=1e-3)

model.compile(loss='binary_crossentropy', 
              optimizer=adam,metrics=['accuracy','mse',
                                      AUC(name='auc'),
                                      Precision(name='precision'),
                                      KLDivergence(name='kldiv')
                                      ]
              )

model.summary()
plot_model(model, show_shapes=True, show_layer_names=True, to_file='covnet_model.png')
open('covnet_model.json','w').write(json.dumps(model.to_json(), indent = 4))


# ========================= #
# ==== Prepare Samples ==== #
# ========================= #

def srt(x):
    return int(x.split('_')[-1].split('.')[0])

path = '/mt/batch/jaraz/RNN_CNN/'
train_dict = {'image': {'bkg' : glb(path+'ENNDataset/train/QCD_CovNet_*'),
                        'sig' : glb(path+'ENNDataset/train/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(path+'ENNDataset/train/QCD_LSTM_*'),
                        'sig' : glb(path+'ENNDataset/train/tt_LSTM_*')}}

val_dict   = {'image': {'bkg' : glb(path+'ENNDataset/val/QCD_CovNet_*'),
                        'sig' : glb(path+'ENNDataset/val/tt_CovNet_*')},
              'lstm' : {'bkg' : glb(path+'ENNDataset/val/QCD_LSTM_*'),
                        'sig' : glb(path+'ENNDataset/val/tt_LSTM_*')}}

test_dict   = {'image': {'bkg' : glb(path+'ENNDataset/test/QCD_CovNet_*'),
                         'sig' : glb(path+'ENNDataset/test/tt_CovNet_*')},
               'lstm' : {'bkg' : glb(path+'ENNDataset/test/QCD_LSTM_*'),
                         'sig' : glb(path+'ENNDataset/test/tt_LSTM_*')}}

for dc in [train_dict, val_dict, test_dict]:
    dc['image']['bkg'].sort(key=srt)
    dc['image']['sig'].sort(key=srt)
    dc['lstm']['bkg'].sort(key=srt)
    dc['lstm']['sig'].sort(key=srt)


def normalize_meanstd(image):
    #mean    = np.mean(image,axis=(1,2), keepdims=True)
    #stddev  = np.sqrt(((image - mean)**2).mean((1,2), keepdims=True))
    #adjusted_stddev = max(stddev, 1.0/np.sqrt(image.shape[0]))
    #return (image-mean)/adjusted_stddev
    return image


train = DataGenerator(train_dict,batch_size=128,   std=normalize_meanstd)
val   = DataGenerator(val_dict,  batch_size=500,   std=normalize_meanstd)

try:
    model.fit(train,
              epochs=100,
              verbose=1,
              validation_data = val,
              validation_freq = 1,
              max_queue_size=10,#max_queue_size,  # defaults to 10
              callbacks = [
                  ModelCheckpoint('covnet_model.h5', monitor='val_loss', verbose=True, save_best_only=True),
                  # EarlyStopping(verbose=1, patience=50, monitor='val_auc', 
                  #               mode='min',min_delta=0.001),
                  ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=25, verbose=1, 
                                    mode='min', min_delta=0.01, cooldown=0, min_lr=1e-8),
                  TerminateOnNaN()
                  ]
              )
except KeyboardInterrupt:
    print('   * Training stopped by the user.')

model.load_weights('covnet_model.h5')

train.Reset()
val.Reset()

plt.plot(model.history.history['loss'])
plt.plot(model.history.history['val_loss'])
plt.plot(model.history.history['accuracy'])
plt.plot(model.history.history['val_accuracy'])
plt.plot(model.history.history['mse'])
plt.plot(model.history.history['val_mse'])
plt.plot(model.history.history['auc'])
plt.plot(model.history.history['val_auc'])
plt.yscale('log')
plt.title('Training History')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['training', 'validation','accuracy', 
            'validation acc.','mse','val_mse',
            'auc','val_auc'], loc='best')
plt.savefig('covnet_val_loss.png', bbox_inches = 'tight')
plt.close()


plt.plot(model.history.history['kldiv'])
plt.plot(model.history.history['val_kldiv'])
plt.yscale('log')
plt.title('Training History')
plt.xlabel('epoch')
plt.legend(['KL divergence', 'val KL divergence'], loc='best')
plt.savefig('covnet_KL.png', bbox_inches = 'tight')
plt.close()

model_history = pd.DataFrame.from_dict(model.history.history)
model_history.to_csv('covnet_history.csv')

test = DataGenerator(test_dict,batch_size=50000,
                     shuffle=True,test=True,std=normalize_meanstd)

yhat = model.predict(test[0],verbose=True)
y_test = test.y_test()

test.Reset()

fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
# sys1 = fig.add_subplot()
binss = np.linspace(0,1,15)
bkg = plt.hist(yhat[np.array(y_test) == 0],
         histtype='bar',  bins=binss, 
         label=r'Background',density=True,
         linewidth=0, color='tab:blue', linestyle='solid')
sig = plt.hist(yhat[np.array(y_test) == 1],
         histtype='step', bins=binss, 
         label=r'Signal',
         color=None, edgecolor='tab:red', linewidth=2, linestyle="dashed",
         bottom=None, cumulative=False, density=True, align="mid", orientation="vertical")
leg = plt.legend(loc='upper right',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
plt.xlabel('Classifier Output',fontsize=20)
plt.ylabel('Normalized Number of Events',fontsize=20)
plt.axis([0,1,1e-2,150])
plt.yscale("log", nonposy='clip')
plt.savefig('covnet_class_output.png', bbox_inches = 'tight')




from sklearn.metrics import roc_curve, auc
## Plot ROC
# Signal labal is 0 

test.test = False

fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
# sys2 = fig.add_subplot()

# sys2.plot(tpr, 1./fpr,color='tab:blue',lw=2,
#           label=r'ROC (AUC = ${:.3f}$)'.format(roc_auc))
roc_dict= {}
avg_bias = 0.
avg_var  = 0.
for ix in range(len(test)):
    X_test, y_test = test[ix]
    yhat = model.predict(X_test,verbose=True)

    fpr, tpr, tresholds = roc_curve(y_test, yhat,pos_label=1, 
                                    drop_intermediate=False)
    roc_auc = auc(fpr, tpr)
    scores = model.evaluate(X_test,y_test,verbose=0,batch_size=100,return_dict=True)
    for key,item in scores.items():
        roc_dict[key+'_'+str(ix)] = float(item)
    roc_dict['tpr_'+str(ix)] = tpr.tolist() if type(tpr)==np.ndarray else tpr
    roc_dict['fpr_'+str(ix)] = fpr.tolist() if type(fpr)==np.ndarray else fpr
    roc_dict['tresholds_'+str(ix)] = tresholds.tolist() if type(tresholds)==np.ndarray else tresholds
    roc_dict['auc'+str(ix)] = float(roc_auc)
    plt.plot(tpr,1./fpr,lw=2,
              label=r'ROC (AUC = ${:.3f}$)'.format(roc_auc))
x = np.linspace(0.01,1.,100)

plt.plot(x, 1./x, 'k--', lw=2)

plt.yscale('log')
plt.ylabel('Background Rejection ($1/\epsilon_B$)',fontsize=20)
plt.xlabel('True Positive Rate ($\epsilon_S$)',fontsize=20)
leg = plt.legend(loc='best',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
#plt.savefig(os.path.join(path,'roc_curve.png'),bbox_inches='tight')
plt.savefig('covnet_roc.png', bbox_inches = 'tight')

plot_correlations(get_correlations(X_test,yhat),
                  extent=[-1.6,1.6,-1.6,1.6],
                  title='Correlation between pixels & the CNN prediction',name='covnet_corr.png')

open('covnet_roc.json','w').write(json.dumps(roc_dict, indent = 4))

# test.Reset()
# train.batch_size = 25000
# test.batch_size  = 25000
# X_train, y_train = train[0]
# X_test,  y_test  = test[0]
# avg_expected_loss, avg_bias, avg_var = bias_variance_decomp(model, 
#                                                             X_train, y_train,
#                                                             X_test, y_test,
#                                                             loss='mse',
#                                                             num_rounds=100,
#                                                             random_seed=123,
#                                                             epochs=50,
#                                                             verbose=1)
# print('Average expected loss: %.3f' % avg_expected_loss)
# print('Average bias: %.3f' % avg_bias)
# print('Average variance: %.3f' % avg_var)