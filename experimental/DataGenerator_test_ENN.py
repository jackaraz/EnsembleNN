#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 21 10:11:17 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np


from tensorflow.keras.utils import Sequence
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dropout, Conv2D, MaxPooling2D, Flatten, LSTM, Input, Dense
from tensorflow.keras.layers import concatenate
from tensorflow.keras import metrics
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN
import matplotlib.pyplot as plt
from TopTagger.ensemble.DataGenerator import DataGenerator

from glob import glob as glb
import os


path = '/mt/batch/jaraz/RNN_CNN/'
def srt_image(x):
    return x.split('_')[4]
def srt_lstm(x):
    return x.split('_')[7]

img_bkg = glb(path+'ANALYSIS_1/images2/train/QCD_train_*_10000_norm.npz')
img_bkg.sort(key=srt_image)
img_sig = glb(path+'ANALYSIS_1/images2/train/tt_train_*_10000_norm.npz')
img_sig.sort(key=srt_image)

lstm_bkg = glb(path+'LSTM_dataset/train/QCD_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_bkg.sort(key=srt_image)
lstm_sig = glb(path+'LSTM_dataset/train/tt_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_sig.sort(key=srt_lstm)

min_smp_size = min([len(img_bkg),len(img_sig),len(lstm_bkg),len(lstm_sig)])

train = {'image': {'bkg' : img_bkg[:min_smp_size],
                   'sig' : img_sig[:min_smp_size]},
         'lstm' : {'bkg' : lstm_bkg[:min_smp_size],
                   'sig' : lstm_sig[:min_smp_size]}}

img_bkg = glb(path+'ANALYSIS_1/images2/val/QCD_val_*_10000_norm.npz')
img_bkg.sort(key=srt_image)
img_sig = glb(path+'ANALYSIS_1/images2/val/tt_val_*_10000_norm.npz')
img_sig.sort(key=srt_image)

lstm_bkg = glb(path+'LSTM_dataset/val/QCD_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_bkg.sort(key=srt_image)
lstm_sig = glb(path+'LSTM_dataset/val/tt_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_sig.sort(key=srt_lstm)

min_smp_size = min([len(img_bkg),len(img_sig),len(lstm_bkg),len(lstm_sig)])

val   = {'image': {'bkg' : img_bkg[:min_smp_size],
                   'sig' : img_sig[:min_smp_size]},
         'lstm' : {'bkg' : lstm_bkg[:min_smp_size],
                   'sig' : lstm_sig[:min_smp_size]}}

train = DataGenerator(train,batch_size=128)
val   = DataGenerator(val,  batch_size=1000)

steps_per_epoch = 10000*2//128
val_steps_per_epoch = 10000*2//1000
max_queue_size  = steps_per_epoch//40
print('steps per epoch = ',steps_per_epoch,"max queue size = ",max_queue_size)


# CovNet -> DeepTop
cov_input    = Input(shape=(37,37,1),name='CovNet')
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_input)
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_hidden)
cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_hidden)
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_hidden)
cov_hidden   = Flatten()(cov_hidden)
cov_hidden   = Dense(64, activation='relu')(cov_hidden)
cov_hidden   = Dense(64, activation='relu')(cov_hidden)
cov_hidden   = Dense(64, activation='relu')(cov_hidden)
cov_output   = Dense(1, activation='sigmoid')(cov_hidden)
CovNet_model = Model(inputs=cov_input, outputs=cov_output)


# LSTM
lstm_input = Input(shape=(40,1), name='LSTM')
lstm_hidden = LSTM(10, activation="tanh", 
                   recurrent_activation='hard_sigmoid')(lstm_input)
lstm_hidden = Dropout(0.25)(lstm_hidden)
lstm_output = Dense(1, activation='sigmoid')(lstm_hidden)
LSTM_model  = Model(inputs=lstm_input, outputs=lstm_output)

# Merge
merged_output = concatenate([CovNet_model.output, LSTM_model.output], axis=-1)
merged_model  = Dense(1, activation='sigmoid')(merged_output)

model = Model(inputs=[CovNet_model.input, LSTM_model.input], outputs=merged_model)

model.compile(loss='binary_crossentropy', optimizer='adam',metrics=['accuracy'])
model.summary()

print('fitting the model now...')
# model.fit_generator(generator=train.generate(),
#                      steps_per_epoch = steps_per_epoch,
#                      epochs=50,
#                      verbose=1,
#                      validation_data = val.generate(),
#                      validation_steps = val_steps_per_epoch,
#                      max_queue_size=10,#max_queue_size,  # defaults to 10
#                      use_multiprocessing=True,
#                      callbacks = [
#                          EarlyStopping(monitor='val_loss', patience=10, verbose=1),
#                          ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1),
#                          TerminateOnNaN()
#                          ]
#                     )
#train.on_epoch_end()
#val.on_epoch_end()
import sys
def progress(count, total, status='Complete',epoch=-1, train=[],val=[]):
    bar_len = 30
    filled_len = int(round(bar_len * count / float(total)))
    if len(train)>0:
        status += ' loss = {:.5f} , acc = {:.2%}'.format(train[0],train[1])
    if any([x>=0 for x in val]):
        status += ' val_loss = {:.5f} , val_acc = {:.2%}'.format(val[0],val[1])
    percents = round(100.0 * count / float(total), 1)
    bar = '█' * filled_len + '-' * (bar_len - filled_len)
    if epoch>=0:
        sys.stdout.write('Epoch %s : |%s| \x1b[32m%s%s\x1b[0m %s\r' % \
                                                    (epoch,bar, percents, '%', status.ljust(50)))
    else:
        sys.stdout.write('Progress : |%s| \x1b[32m%s%s\x1b[0m %s\r' % \
                                                    (bar, percents, '%', status.ljust(50)))
    sys.stdout.flush()



history = {'loss':[],'acc':[],'val_loss':[],'val_acc':[]}
i=0
val_loss, val_acc = -1, -1
while i<=50:
    for ix in range(len(train)):
        X,y = train[ix]
        # print(model.input_shape)
        if X[0].shape[1:] != model.input_shape[0][1:] or X[1].shape[1:] != model.input_shape[1][1:] or len(y) != X[0].shape[0]:
            # print('\n   * Issue with the input shape ',X[0].shape,X[1].shape, len(y))
            # print('      - ','\n      - '.join(train.get_current_file_set()))
            continue
        loss, acc = model.train_on_batch(X,y,reset_metrics=True)
        progress(ix,len(train),epoch=i+1,
                 train= [loss,acc], val=[val_loss,val_acc])
    history['loss'].append(loss)
    history['acc'].append(acc)
    # print('\n   loss = {:.5f}, acc = {:.2%}'.format(loss,acc))
    # print('\n   Calculating Validation metrics...')
    for ix in range(len(val)):
        #progress(ix,len(val),epoch=i)
        X,y = val[ix]
        if X[0].shape[1:] != (37,37,1) or X[1].shape[1:] != (40,1):
            print('\n   * Issue with the input shape ',X[0].shape,X[1].shape)
            print('      - ','\n      - '.join(val.get_current_file_set()))
            continue
        val_loss, val_acc = model.test_on_batch(X,y=y,reset_metrics=True)
    if len(history['val_loss']) > 0:
        if val_loss < min(history['val_loss']):
            print('\nVal loss has improved {:.5f} -> {:.5f}\n'.format(min(history['val_loss']),val_loss))
    history['val_loss'].append(val_loss)
    history['val_acc'].append(val_acc)
    train.on_epoch_end()
    val.on_epoch_end()
    i+=1


# current_learning_rate = calculate_learning_rate(counter)
# # train model:
# K.set_value(model.optimizer.learning_rate, current_learning_rate)  # set new learning_rate
# loss = model.train_on_batch(x, y) 
#model.save_weights

plt.plot(history['loss'])
plt.plot(history['val_loss'])
plt.plot(history['acc'])
plt.plot(history['val_acc'])
#plt.yscale('log')
plt.title('Training History')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['training', 'validation','accuracy'], loc='best')
plt.savefig('val_loss.png')



img_bkg = glb(path+'ANALYSIS_1/images2/test/QCD_test_*_10000_norm.npz')
img_bkg.sort(key=srt_image)
img_sig = glb(path+'ANALYSIS_1/images2/test/tt_test_*_10000_norm.npz')
img_sig.sort(key=srt_image)

lstm_bkg = glb(path+'LSTM_dataset/test/QCD_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_bkg.sort(key=srt_image)
lstm_sig = glb(path+'LSTM_dataset/test/tt_ptJ_mj_mmdt_dkTlead_*.npz')
lstm_sig.sort(key=srt_lstm)

min_smp_size = min([len(img_bkg),len(img_sig),len(lstm_bkg),len(lstm_sig)])

test   = {'image': {'bkg' : img_bkg[:min_smp_size],
                    'sig' : img_sig[:min_smp_size]},
          'lstm' : {'bkg' : lstm_bkg[:min_smp_size],
                    'sig' : lstm_sig[:min_smp_size]}}


test = DataGenerator(test,128,shuffle=False, test=True)
steps_per_epoch = 10000*4//100
max_queue_size  = steps_per_epoch//10
print('steps per epoch = ',steps_per_epoch,"max queue size = ",max_queue_size)



yhat=[]
for ix in range(len(test)):
    current_yhat = model.predict_on_batch(test[ix])
    yhat += current_yhat.tolist()
yhat   = np.array(yhat)
y_test = test.y_test()

fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
# sys1 = fig.add_subplot()
binss = np.linspace(0,1,15)
bkg = plt.hist(yhat[np.array(y_test) != 1],
         histtype='bar',  bins=binss, 
         label=r'Background',density=True,
         linewidth=0, color='tab:blue', linestyle='solid')
sig = plt.hist(yhat[np.array(y_test) == 1],
         histtype='step', bins=binss, 
         label=r'Signal',
         color=None, edgecolor='tab:red', linewidth=2, linestyle="dashed",
         bottom=None, cumulative=False, density=True, align="mid", orientation="vertical")
leg = plt.legend(loc='upper right',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
# plt.xaxis.set_major_locator(MultipleLocator(0.2))
# plt.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
# plt.xaxis.set_minor_locator(MultipleLocator(0.2/5.))
plt.xlabel('Classifier Output',fontsize=20)
plt.ylabel('Normalized Number of Events',fontsize=20)
plt.axis([0,1,1e-2,150])
plt.yscale("log", nonposy='clip')
plt.savefig('class_out.png')

from sklearn.metrics import roc_curve, auc
## Plot ROC
# Signal labal is 0 
fpr, tpr, tresholds = roc_curve(y_test, yhat,pos_label=1, 
                                drop_intermediate=False)
# fpr, tpr, tresholds = roc_curve(y_test, yhat[:,0])
roc_auc = auc(fpr, tpr)

fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
# sys2 = fig.add_subplot()

# sys2.plot(tpr, 1./fpr,color='tab:blue',lw=2,
#           label=r'ROC (AUC = ${:.3f}$)'.format(roc_auc))
plt.plot(fpr, tpr,color='tab:blue',lw=2,
          label=r'ROC (AUC = ${:.3f}$)'.format(roc_auc))

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.axis([0,1,0,1.05])
# sys2.set_xlim((0, 1))
# sys2.set_ylim((1, 1000))
# sys2.set_yscale('log')
plt.xlabel('False Positive Rate ($\epsilon_B$)',fontsize=20)
plt.ylabel('True Positive Rate ($\epsilon_S$)',fontsize=20)
# plt.xaxis.set_major_locator(MultipleLocator(.2))
# plt.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
# plt.xaxis.set_minor_locator(MultipleLocator(.2/5.))
# plt.yaxis.set_major_locator(MultipleLocator(.2))
# plt.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
# plt.yaxis.set_minor_locator(MultipleLocator(.2/5.))
leg = plt.legend(loc='best',fontsize=12)
leg.get_frame().set_alpha(0.5)
leg.get_frame().set_linewidth(0.0)
leg.set_zorder(100)
#plt.savefig(os.path.join(path,'roc_curve.png'),bbox_inches='tight')
plt.savefig('roc.png')

