#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 14:15:58 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np
np.warnings.filterwarnings('ignore',category=np.VisibleDeprecationWarning)
from tensorflow.keras.utils import Sequence
import os

"""
    https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
    https://machinelearningmastery.com/how-to-load-large-datasets-from-directories-for-deep-learning-with-keras/
    Generates data for Keras
"""

class DataSample:
    def __init__(self,data_tuple):
        self.X_CovNet = []
        self.X_LSTM   = []
        self.y        = []
        self.__reset_iteration()
        for cov, lstm, truth in data_tuple:
            self.X_CovNet.append(cov)
            self.X_LSTM.append(lstm)
            self.y.append(truth)

    def __iter__(self):
        self.__reset_iteration()
        return self

    def __next__(self):
        try:
            if self.n < len(self.y):
                result = (self.X_CovNet[self.n], self.X_LSTM[self.n], self.y[self.n])
                self.n += 1
                return result
            else:
                return (-1,-1,-1)
        except:
            return (-1,-1,-1)

    def next(self): # for py2.7
        return self.__next__()

    def __reset_iteration(self):
        self.n = 0



class DataGenerator(Sequence):
    def __init__(self,smp_path_dict,batch_size=32, shuffle=True,test=False,shape=[(37,37,1),(40,1)],
                 std=[None,None]):
        """
        Parameters
        ----------
        smp_path_dict : Dictionary
            Needs to be a dictionary including full arranged paths of all signal and 
            background samples.
            smp_path_dict = {'image' : {'sig': [LIST OF SIGNAL SMP], 'bkg': [LIST OF BKG SMP]},
                             'lstm': {SAME AS IMAGE}}}
        batch_size : INT, optional
            Size of the batch to be returned. The default is 32.
        shuffle : BOOL, optional
            Should the sample be shuffled. The default is True.
        test : BOOL, optional
            Is this a test sample in that case code will save truth values. 
            The default is False.

        Returns
        -------
        None.
        """
        self.image_paths = smp_path_dict['image']
        self.lstm_paths  = smp_path_dict['lstm' ]
        self.batch_size  = batch_size
        self.shuffle     = shuffle
        self.test        = test
        self.__initialize_samples()
        self.size        = len(self)
        self.init        = True
        self.image_shape = shape[0]
        self.lstm_shape  = shape[1]

        assert len(std)==2, 'There should be two input in the standardize list.'
        assert all([(x == None or hasattr(x,'__call__')) for x in std]), \
                'Standardization has to be either a function or None.'

        self.std         = std
        self.datasample  = []
        # self.on_epoch_end()

    def Reset(self):
        self.datasample  = []
        self.init        = True
        self.nfile       = 0


    def __initialize_samples(self):
        ' Prepare sample containers, they need to be ordered in advance'
        self.sample_size = 0
        self.signal_size = 0
        self.bkg_size    = 0
        file_set = []
        for fl_sig, fl_bkg, fl_sigl, fl_bkgl in zip(self.image_paths['sig'],self.image_paths['bkg'],
                                                    self.lstm_paths['sig'],self.lstm_paths['bkg']):
            if not all([os.path.isfile(x) for x in [fl_sig, fl_bkg, fl_sigl, fl_bkgl]]):
                continue

            nsig = np.load(fl_sig)['shape'][0]
            nbkg = np.load(fl_bkg)['shape'][0]
            nsls, nbls = np.load(fl_sigl)['shape'][0], np.load(fl_bkgl)['shape'][0]

            min_size_sig = min([nsig,nsls])
            min_size_bkg = min([nbkg, nbls])

            self.sample_size += min_size_sig+min_size_bkg
            self.signal_size += min_size_sig
            self.bkg_size    += min_size_bkg

            file_set.append((fl_sig, fl_bkg, fl_sigl, fl_bkgl))

        self.samples = file_set


    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size


    def __get_current_file_set(self):
        return self.samples[self.nfile]


    def __getitem__(self,index):
        """
        Parameters
        ----------
        index : INT
            Index not important just index < len(DataGenerator).

        Returns
        -------
        TUPLE or LIST
            If this is a test sample a list of Xs will be returned,
            but if this is a train sample a tuple containing list of Xs and 
            y will be returned.
        """
        # assert index < len(self) , 'index out of range...'
        if self.init:
            self.on_epoch_end()
            self.init = False

        # Generate data
        X_CovNet, X_LSTM, y = self.__data_generation()

        if not self.test:
            return [X_CovNet, X_LSTM], y
        else:
            if '_y' not in list(self.__dict__.keys()):
                self._y = []
            self._y += y.tolist()
            return [X_CovNet, X_LSTM]


    def y_test(self):
        if '_y' in list(self.__dict__.keys()):
            return self._y
        else:
            return


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.nfile = 0
        if self.shuffle == True:
            np.random.shuffle(self.samples)
        self.__read_data()


    def __read_data(self):
        sgim, bkim, sglstm, bklstm = self.__get_current_file_set()
        signal_image = np.load(sgim)['sample']
        bkg_image    = np.load(bkim)['sample']
        sig_lstm     = np.load(sglstm)['sample']
        bkg_lstm     = np.load(bklstm)['sample']

        min_size_sig = min([signal_image.shape[0], sig_lstm.shape[0]])
        min_size_bkg = min([bkg_image.shape[0],    bkg_lstm.shape[0]])

        y = []
        y.extend([1]*min_size_sig)
        y.extend([0]*min_size_bkg)

        images = np.vstack((signal_image[:min_size_sig],   bkg_image[:min_size_bkg]))
        lstm   = np.vstack((sig_lstm[:min_size_sig], bkg_lstm[:min_size_bkg]))

        images = images if self.std[0] == None else self.std[0](images)
        lstm   = lstm   if self.std[1] == None else self.std[1](lstm)

        images = images.reshape(images.shape[0],images.shape[1],images.shape[2],1)
        lstm   = lstm.reshape(lstm.shape[0],lstm.shape[1],1)

        data_tuple = list(zip(images,lstm,y))
        if self.shuffle:
            np.random.shuffle(data_tuple)
        data_tuple = np.array(data_tuple)
        self.datasample = iter(DataSample(data_tuple))


    def __data_generation(self):
        'Generates data containing batch_size samples' 
        # Initialization
        # Get file names
        Xcov, Xlstm, y = [],[],[]
        while len(y) < self.batch_size:
            xcov, xlstm, ytruth = next(self.datasample)
            if not (isinstance(xcov,np.ndarray) and isinstance(xlstm,np.ndarray) and (ytruth in [0,1])):
                self.nfile +=1
                if self.nfile == len(self.samples):
                    self.on_epoch_end()
                else:
                    self.__read_data()
                continue
            elif xcov.shape != self.image_shape or xlstm.shape != self.lstm_shape:
                print(' WARNING : Shape does not match!')
                continue
            else:
                Xcov.append(xcov)
                Xlstm.append(xlstm)
                y.append(ytruth)
        return np.array(Xcov), np.array(Xlstm), np.array(y)
