#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 13:53:57 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np

class DataGenerator(object):
    # print('Generates data for Keras')
    def __init__(self, sg_files, bg_files,
                 train_frac_rel, in_val_frac_rel,in_test_frac_rel, set_label,
                 batch_size = 128, shuffle = True, sample_relative_size=1.):
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.y_test = []
        self.signal_files = sg_files
        self.bkg_files = bg_files
        self.sample_frac = [train_frac_rel, in_val_frac_rel,in_test_frac_rel]
        self.label = set_label
        self.rel_size = sample_relative_size

    def generate(self):
        while True:
            indexes = np.arange(len(self.signal_files))
            if self.shuffle:
                indexes = np.random.permutation(indexes)

            for index in indexes:

                signal     = np.load(self.signal_files[index])['image']
                background = np.load(self.bkg_files[index])['image']

                signal     = cut_sample(signal,     self.rel_size)
                background = cut_sample(background, self.rel_size)

                data_in= add_sig_bg_true_value(signal,background,shuffle=self.shuffle)
                data_tuple = sets(data_in)

                x_train1, y_train1, x_val1, y_val1, x_test1, y_test1 = split_sample(data_tuple, 
                                                                                    self.sample_frac[0],
                                                                                    self.sample_frac[1],
                                                                                    self.sample_frac[2])

                if self.label == 'train':
                    subindex= np.arange(len(x_train1))
                    imax = int(len(subindex)/self.batch_size)
                elif self.label == 'val':
                    subindex= np.arange(len(x_val1))
                    imax = int(len(subindex)/self.batch_size)
                elif self.label == 'test':
                    subindex= np.arange(len(x_test1))
                    imax = int(len(subindex)/self.batch_size)

                for i in range(imax):
                    if self.label=='train':
                        x_train_temp = x_train1[i*self.batch_size:(i+1)*self.batch_size]
                        y_train_temp = y_train1[i*self.batch_size:(i+1)*self.batch_size]
                        yield x_train_temp, y_train_temp

                    elif self.label == 'val':
                        x_val_temp = x_val1[i*self.batch_size:(i+1)*self.batch_size]
                        y_val_temp = y_val1[i*self.batch_size:(i+1)*self.batch_size]
                        yield x_val_temp, y_val_temp

                    elif self.label=='test':
                        self.y_test += np.argmax(y_test1[i*self.batch_size:(i+1)*self.batch_size],axis=1).tolist()
                        yield x_test1[i*self.batch_size:(i+1)*self.batch_size]
