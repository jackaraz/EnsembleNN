#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 22:01:38 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models       import Model
from tensorflow.keras.layers       import Conv2D, MaxPooling2D, Flatten, Input
from tensorflow.keras.layers       import Dense, BatchNormalization, Dropout
# from tensorflow.keras.regularizers import l2

# inspired by 1803.00107

# CovNet -> DeepTopXL
cov_input    = Input(shape=(37,37,1),name='DeepTopXL')
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_input)
cov_hidden   = BatchNormalization()(cov_hidden)
cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
cov_hidden   = Flatten()(cov_hidden)
cov_hidden   = Dense(16, activation='relu')(cov_hidden)
cov_output   = Dropout(0.25)(cov_hidden)

output  = Dense(1, activation='sigmoid', name='Final_Output')(cov_output)

DeepTopXS = Model(inputs=cov_input, outputs=output)

#Cov 32 Dense 64
