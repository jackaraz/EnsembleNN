#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 20:46:26 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""


"""
Notes on the architecture 
    based on https://github.com/YaleATLAS/CERNDeepLearningTutorial/blob/master/jet_images.ipynb

Sun May 24 21:14:40 2020: 
    Larger kernel size seems to help, we are getting large improvement
    in the accuracy where current model gives upto 85% cross val accuracy.
    Also LeakyRelu and drop out addition after each layer seems to help more than
    MaxPooling option. Down side is the loss value, which is currently extremely large
    this might be possible to improve by adding dense layers at the end of the 
    architecture. 
    PS: After the training the test accuracy was 50% again.
Wed May 27 20:17:39 2020
    As soon as I added activation and padding to each layer (there has to be an activation default is none)
    the cross val accuracy droped to 50%. 

"""

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, LeakyReLU, Dropout


input_shape = (37, 37, 1)

TauTagger = Sequential()

TauTagger.add(Conv2D(32, kernel_size=7, strides=1, 
                     input_shape=input_shape))
TauTagger.add(LeakyReLU())
TauTagger.add(Dropout(0.2))

TauTagger.add(Conv2D(64, kernel_size=7, strides=1))
TauTagger.add(LeakyReLU())
TauTagger.add(Dropout(0.2))

TauTagger.add(Conv2D(128, kernel_size=5, strides=1))
TauTagger.add(LeakyReLU())
TauTagger.add(Dropout(0.2))

TauTagger.add(Conv2D(256, kernel_size=5, strides=1))
TauTagger.add(LeakyReLU())
TauTagger.add(Flatten())
TauTagger.add(Dropout(0.2))

TauTagger.add(Dense(2, activation='softmax'))
