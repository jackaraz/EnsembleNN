#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 14:30:47 2021

@author : Jack Y. Araz <jackaraz@gmail.com>
"""


from tensorflow.keras.models       import Model
from tensorflow.keras.layers       import Conv2D, MaxPooling2D, Flatten, Input, concatenate
from tensorflow.keras.layers       import Dense, BatchNormalization, LSTM, Dropout
from tensorflow.keras.regularizers import l2
import tensorflow_probability      as tfp
import tensorflow                  as tf

def BayesModel(NUM_TRAIN_EXAMPLES):
    kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) / tf.cast(NUM_TRAIN_EXAMPLES, dtype=tf.float32))

    # CovNet -> DeepTopXL
    cov_input    = Input(shape=(37,37,1),name='DeepTopXL')
    cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_input)
    cov_hidden   = BatchNormalization()(cov_hidden)
    cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
    cov_hidden   = Flatten()(cov_hidden)
    cov_output   = Dense(16, activation='relu')(cov_hidden)

    # LSTM
    lstm_input  = Input(shape=(40,1), name='LSTM')
    lstm_hidden = LSTM(64, activation="tanh", 
                       recurrent_activation='sigmoid')(lstm_input)

    lstm_hidden = Dense(64, activation='relu')(lstm_hidden)
    lstm_hidden = Dropout(0.25)(lstm_hidden)
    lstm_hidden = Dense(64, activation='relu')(lstm_hidden)
    lstm_hidden = Dropout(0.25)(lstm_hidden)
    lstm_hidden = Dense(32, activation='relu')(lstm_hidden)
    LSTM_output = Dropout(0.25)(lstm_hidden)

    # Concantenate
    merged = concatenate([cov_output, LSTM_output], name='Concatenated')
    merged = Dropout(0.25)(merged)
    merged = Dense(96, activation='relu',kernel_regularizer=l2(0.05),name='L2005')(merged)
    merged = Dropout(0.25)(merged)

    merged_output  = tfp.layers.DenseFlipout(1, 
                                             activation=tf.nn.sigmoid, 
                                             kernel_divergence_fn = kl_divergence_function,
                                             name='Final_Output')(merged)


    return Model(inputs=[cov_input, lstm_input], outputs=merged_output)