#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 22:01:38 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""
from tensorflow.keras.models                import Model
from tensorflow.keras.layers                import MaxPooling2D, Flatten, Input
from tensorflow.keras.layers                import BatchNormalization, Dropout
import tensorflow_probability               as tfp
import tensorflow                           as tf

def BayesModel(NUM_TRAIN_EXAMPLES):

    kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) / tf.cast(NUM_TRAIN_EXAMPLES, dtype=tf.float32))

    cov_input    = Input(shape=(37,37,1),name='BayesianConvolutional')

    cov_hidden   = tfp.layers.Convolution2DFlipout(8, (4,4),  padding='same',
                                        activation           = tf.nn.relu,
                                        kernel_divergence_fn = kl_divergence_function)(cov_input)

    cov_hidden   = BatchNormalization()(cov_hidden)
    cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
    cov_hidden   = Flatten()(cov_hidden)

    cov_hidden   = tfp.layers.DenseFlipout(16, 
                                           activation           = tf.nn.relu,
                                           kernel_divergence_fn = kl_divergence_function)(cov_hidden)

    cov_output   = Dropout(0.25)(cov_hidden)

    output       = tfp.layers.DenseFlipout(1, name='Final_Output',
                                           activation           = tf.nn.sigmoid,
                                           kernel_divergence_fn = kl_divergence_function)(cov_output)

    return Model(inputs=cov_input, outputs=output)

