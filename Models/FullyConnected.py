#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 21:25:25 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""


"""
Notes on the architecture 
    based on https://github.com/YaleATLAS/CERNDeepLearningTutorial/blob/master/jet_images.ipynb

"""

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, LeakyReLU, Dropout


input_shape = (37, 37, 1)

FullyConnected = Sequential()

FullyConnected.add(Flatten(input_shape=input_shape))

FullyConnected.add(Dense(37 ** 2, kernel_initializer='he_normal'))
FullyConnected.add(LeakyReLU())
FullyConnected.add(Dropout(0.2))

FullyConnected.add(Dense(512, kernel_initializer='he_normal'))
FullyConnected.add(LeakyReLU())
FullyConnected.add(Dropout(0.2))

FullyConnected.add(Dense(256, kernel_initializer='he_normal'))
FullyConnected.add(LeakyReLU())
FullyConnected.add(Dropout(0.2))

FullyConnected.add(Dense(128, kernel_initializer='he_normal'))
FullyConnected.add(LeakyReLU())
FullyConnected.add(Dropout(0.2))

FullyConnected.add(Dense(2, activation='softmax'))