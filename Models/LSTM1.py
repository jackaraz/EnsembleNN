#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 14:26:33 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

from keras.layers import LSTM, Dropout, Dense
from keras.models import Sequential

input_shape = (40,1)

LSTM1 = Sequential()

LSTM1.add(LSTM(40, input_shape=input_shape,activation="tanh", 
               recurrent_activation='hard_sigmoid'))
LSTM1.add(Dense(2, activation='softmax'))
