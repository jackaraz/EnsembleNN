#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 21:16:02 2021

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models  import Model
from tensorflow.keras.layers  import Dropout, LSTM, Input, Dense
import tensorflow_probability as tfp
import tensorflow             as tf

def BayesModel(NUM_TRAIN_EXAMPLES):

    kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) / tf.cast(NUM_TRAIN_EXAMPLES, dtype=tf.float32))

    # LSTM
    lstm_input = Input(shape=(40,1), name='semiBRNN')
    lstm_hidden = LSTM(64, activation="tanh", 
                       recurrent_activation='sigmoid')(lstm_input)
    
    hidden = Dense(64, activation = tf.nn.relu)(lstm_hidden)
    hidden = Dropout(.25)(hidden)
    hidden = Dense(64, activation = tf.nn.relu)(hidden)
    hidden = Dropout(.25)(hidden)
    hidden = Dense(32, activation = tf.nn.relu)(hidden)
    hidden = Dropout(.25)(hidden)
    
    LSTM_output = tfp.layers.DenseFlipout(1, 
                                          activation           = tf.nn.sigmoid,
                                          kernel_divergence_fn = kl_divergence_function,
                                          name='Final_Output')(hidden)
    
    return Model(inputs=lstm_input, outputs=LSTM_output)