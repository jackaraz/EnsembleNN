#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 09:31:33 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from keras.models import Sequential
from keras.layers import Dense, Flatten


input_shape = (37, 37, 1)

Flat = Sequential()

Flat.add(Flatten(input_shape=input_shape))

Flat.add(Dense(1069, activation='relu'))
Flat.add(Dense(128, activation='relu'))
Flat.add(Dense(64, activation='relu'))

Flat.add(Dense(2, activation='softmax'))