#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 18:59:55 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""


"""
Notes on the architecture 

Based on 1803.00107

Sun May 24 21:29:15 2020
    Terrible accuracy, would be better if I flip a coin...

Mon May 25 13:44:12 2020
    Increasing the kernel size. First 2 Conv -> 7x7 rest -> 5x5
Mon May 25 13:58:29 2020
    Kernel size does not change anything in the accuracy, its still 50%
    adding leakyrelu and dropout after each layer. This doesn't help as well
    increasing the number of features.
    -> Doesnt help either. Still 50% accuracy.
Mon May 25 15:45:24 2020
    Removing MaxPooling, still same
"""


from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten

input_shape = (37, 37, 1)

DeepTop = Sequential()


DeepTop.add(Conv2D(128, (4,4), activation='relu',padding='same',
                   input_shape=input_shape))
DeepTop.add(Conv2D(64, (4,4), activation='relu',padding='same'))

DeepTop.add(MaxPooling2D(pool_size=(2, 2)))

DeepTop.add(Conv2D(64, (4,4), activation='relu',padding='same'))
DeepTop.add(Conv2D(64, (4,4), activation='relu',padding='same'))

DeepTop.add(MaxPooling2D(pool_size=(2, 2)))

DeepTop.add(Flatten())

DeepTop.add(Dense(64, activation='relu'))
DeepTop.add(Dense(256, activation='relu'))
DeepTop.add(Dense(256, activation='relu'))

DeepTop.add(Dense(2, activation='softmax'))
