#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 21:18:38 2021

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models       import Model
from tensorflow.keras.layers       import Conv2D, MaxPooling2D, Flatten, Input
from tensorflow.keras.layers       import Dense, BatchNormalization, Dropout
import tensorflow_probability      as tfp
import tensorflow                  as tf


def BayesModel(NUM_TRAIN_EXAMPLES):
    kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) / tf.cast(NUM_TRAIN_EXAMPLES, dtype=tf.float32))

    cov_input    = Input(shape=(37,37,1),name='DeepTopXL')
    cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_input)
    cov_hidden   = BatchNormalization()(cov_hidden)
    cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
    cov_hidden   = Flatten()(cov_hidden)
    cov_hidden   = Dense(16, activation='relu')(cov_hidden)
    cov_output   = Dropout(0.25)(cov_hidden)
    
    output       = tfp.layers.DenseFlipout(1, name='Final_Output',
                                           activation           = tf.nn.sigmoid,
                                           kernel_divergence_fn = kl_divergence_function)(cov_output)
    
    return Model(inputs=cov_input, outputs=output)