#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 11:13:06 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models       import Model
from tensorflow.keras.layers       import Conv2D, MaxPooling2D, Flatten, Input, concatenate
from tensorflow.keras.layers       import Dense, BatchNormalization, LSTM, Dropout
from tensorflow.keras.regularizers import l2
# inspired by 1803.00107

# CovNet -> DeepTopXL
cov_input    = Input(shape=(37,37,1),name='DeepTopXL')
cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_input)
cov_hidden   = BatchNormalization()(cov_hidden)
cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
# cov_hidden   = Conv2D(8, (4, 4), activation='relu',padding='same')(cov_hidden)
cov_hidden   = Flatten()(cov_hidden)
cov_hidden   = Dense(16, activation='relu')(cov_hidden)
cov_output   = Dropout(0.25)(cov_hidden)

# LSTM
lstm_input  = Input(shape=(40,1), name='LSTM')
lstm_hidden = LSTM(64, activation="tanh", 
                   recurrent_activation='sigmoid')(lstm_input)

lstm_hidden = Dense(64, activation='relu')(lstm_hidden)
lstm_hidden = Dropout(0.25)(lstm_hidden)
lstm_hidden = Dense(64, activation='relu')(lstm_hidden)
lstm_hidden = Dropout(0.25)(lstm_hidden)
lstm_hidden = Dense(32, activation='relu')(lstm_hidden)
LSTM_output = Dropout(0.25)(lstm_hidden)

# Concantenate
merged = concatenate([cov_output, LSTM_output], name='Concatenated')
merged = Dropout(0.25)(merged)
merged = Dense(128, activation='relu',kernel_regularizer=l2(0.01), name='L2001')(merged)
merged = Dropout(0.25)(merged)
# merged = Dense(48, activation='relu',kernel_regularizer=l2(0.01))(merged)
# merged = Dropout(0.25)(merged)

merged_output  = Dense(1, activation='sigmoid', name='Final_Output')(merged)


Ensemble = Model(inputs=[cov_input, lstm_input], outputs=merged_output)