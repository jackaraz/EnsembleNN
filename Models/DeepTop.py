#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 17:25:49 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""


"""
Notes on the architecture 

    DeepTop architecture based on 1701.08784

    Sun May 24 21:29:15 2020
        Terrible accuracy, would be better if I flip a coin...

"""

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten

input_shape = (37, 37, 1)

DeepTop = Sequential()

DeepTop.add(Conv2D(8, (4, 4), activation='relu',padding='same',
                   input_shape=input_shape))
DeepTop.add(Conv2D(8, (4, 4), activation='relu',padding='same'))

DeepTop.add(MaxPooling2D(pool_size=(2, 2)))

DeepTop.add(Conv2D(8, (4,4), activation='relu',padding='same'))
DeepTop.add(Conv2D(8, (4,4), activation='relu',padding='same'))

DeepTop.add(Flatten())

DeepTop.add(Dense(64, activation='relu'))
DeepTop.add(Dense(64, activation='relu'))
DeepTop.add(Dense(64, activation='relu'))

DeepTop.add(Dense(2, activation='softmax'))