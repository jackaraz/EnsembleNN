#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 21:19:31 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

"""
Notes on the architecture 
    based on https://github.com/YaleATLAS/CERNDeepLearningTutorial/blob/master/jet_images.ipynb


"""

from keras.models import Sequential
from keras.layers import LocallyConnected2D , Flatten, LeakyReLU,Dropout, Dense

input_shape = (37, 37, 1)

LocallyConnected = Sequential()

LocallyConnected.add(LocallyConnected2D(32, kernel_size=9, strides=2))
LocallyConnected.add(LeakyReLU())
LocallyConnected.add(Dropout(0.2))

LocallyConnected.add(LocallyConnected2D(32, kernel_size=5, strides=1))
LocallyConnected.add(LeakyReLU())
LocallyConnected.add(Dropout(0.2))

LocallyConnected.add(LocallyConnected2D(64, kernel_size=3, strides=1))
LocallyConnected.add(LeakyReLU())
LocallyConnected.add(Dropout(0.2))

LocallyConnected.add(LocallyConnected2D(64, kernel_size=3, strides=1))
LocallyConnected.add(LeakyReLU())
LocallyConnected.add(Flatten())
LocallyConnected.add(Dropout(0.2))

LocallyConnected.add(Dense(2, activation='softmax'))

