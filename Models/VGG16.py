#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 12:55:23 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten

input_shape = (37, 37, 1)

VGG16 = Sequential()

VGG16.add(Conv2D(64, kernel_size=(3,3),
                  activation='relu',padding='same',
                  input_shape=input_shape))
VGG16.add(Conv2D(64, kernel_size=(3,3),
                  activation='relu',padding='same'))

VGG16.add(MaxPooling2D(pool_size=(2, 2)))

VGG16.add(Conv2D(128, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(128, kernel_size=(3,3),
                  activation='relu',padding='same'))

VGG16.add(MaxPooling2D(pool_size=(2, 2)))

VGG16.add(Conv2D(256, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(256, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(256, kernel_size=(3,3),
                  activation='relu',padding='same'))

VGG16.add(MaxPooling2D(pool_size=(2, 2)))

VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))

VGG16.add(MaxPooling2D(pool_size=(2, 2)))

VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))
VGG16.add(Conv2D(512, kernel_size=(3,3),
                  activation='relu',padding='same'))

VGG16.add(MaxPooling2D(pool_size=(2, 2)))

VGG16.add(Flatten())

VGG16.add(Dense(4096, activation='relu'))
VGG16.add(Dropout(0.5))
VGG16.add(Dense(4096, activation='relu'))
VGG16.add(Dropout(0.5))
VGG16.add(Dense(4096, activation='relu'))

VGG16.add(Dense(2, activation='softmax'))
