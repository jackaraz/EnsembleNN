#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 13:15:41 2021

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models       import Model
from tensorflow.keras.layers       import MaxPooling2D, Flatten, Input, concatenate
from tensorflow.keras.layers       import BatchNormalization, LSTM, Dropout
# from tensorflow.keras.regularizers import l2
import tensorflow_probability      as tfp
import tensorflow                  as tf
# inspired by 1803.00107

def BayesModel(NUM_TRAIN_EXAMPLES):
    kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) / tf.cast(NUM_TRAIN_EXAMPLES, dtype=tf.float32))

    # CNN
    cov_input    = Input(shape=(37,37,1),name='DeepTopXL')
    cov_hidden   = tfp.layers.Convolution2DFlipout(8, (4,4),  padding='same',
                                                   activation           = tf.nn.relu,
                                                   kernel_divergence_fn = kl_divergence_function)(cov_input)
    cov_hidden   = BatchNormalization()(cov_hidden)
    cov_hidden   = MaxPooling2D(pool_size=(2, 2))(cov_hidden)
    cov_hidden   = Flatten()(cov_hidden)
    cov_output   = tfp.layers.DenseFlipout(16, 
                                           activation           = tf.nn.relu,
                                           kernel_divergence_fn = kl_divergence_function)(cov_hidden)

    # RNN
    lstm_input  = Input(shape=(40,1), name='LSTM')
    lstm_hidden = LSTM(64, activation="tanh", 
                       recurrent_activation='sigmoid')(lstm_input)
    
    lstm_hidden = tfp.layers.DenseFlipout(64, 
                                          activation           = tf.nn.relu,
                                          kernel_divergence_fn = kl_divergence_function)(lstm_hidden)
    lstm_hidden = Dropout(0.25)(lstm_hidden)
    lstm_hidden = tfp.layers.DenseFlipout(64, 
                                          activation           = tf.nn.relu,
                                          kernel_divergence_fn = kl_divergence_function)(lstm_hidden)
    lstm_hidden = Dropout(0.25)(lstm_hidden)
    lstm_hidden = tfp.layers.DenseFlipout(32, 
                                          activation           = tf.nn.relu,
                                          kernel_divergence_fn = kl_divergence_function)(lstm_hidden)
    LSTM_output = Dropout(0.25)(lstm_hidden)

    # Concantenate
    merged = concatenate([cov_output, LSTM_output], name='Concatenated')
    merged = Dropout(0.25)(merged)
    merged = tfp.layers.DenseFlipout(96, 
                                     activation           = tf.nn.relu,
                                     kernel_divergence_fn = kl_divergence_function)(merged)
    merged = Dropout(0.25)(merged)

    merged_output  = tfp.layers.DenseFlipout(1, 
                                             activation=tf.nn.sigmoid, 
                                             kernel_divergence_fn = kl_divergence_function,
                                             name='Final_Output')(merged)

    return Model(inputs=[cov_input, lstm_input], outputs=merged_output)