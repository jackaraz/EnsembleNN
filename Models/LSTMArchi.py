#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 22:17:50 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dropout, LSTM, Input, Dense

# LSTM
lstm_input = Input(shape=(40,1), name='LSTM')
lstm_hidden = LSTM(64, activation="tanh", 
                   recurrent_activation='sigmoid')(lstm_input)

hidden = Dense(64, activation='relu')(lstm_hidden)
hidden = Dropout(.25)(hidden)
hidden = Dense(64, activation='relu')(hidden)
hidden = Dropout(.25)(hidden)
hidden = Dense(32, activation='relu')(hidden)
hidden = Dropout(.25)(hidden)

LSTM_output = Dense(1, activation='sigmoid', name='Final_Output')(hidden)

LSTM_Model = Model(inputs=lstm_input, outputs=LSTM_output)
