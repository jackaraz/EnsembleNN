#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 18:01:11 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

import fastjet as fj

# take the leading branch of the kTdistance history
def kTdistances_cluster_history(input_jet,input_hist,cut=0.0):
    if input_jet == fj.PseudoJet(0.0,0.0,0.0,0.0) or input_jet.pt() == 0.:
        return []
    this_jet = input_jet
    parent1 = fj.PseudoJet(0.0,0.0,0.0,0.0)
    parent2 = fj.PseudoJet(0.0,0.0,0.0,0.0)
    kTdistances = []
    while True:
        try: # to avoid segmentation fault.
            if not input_hist.has_parents(this_jet,parent1,parent2):
                break
        except:
            break
        kTdist = parent1.kt_distance(parent2)
        if kTdist > cut:
            kTdistances.append(kTdist)
        else:
            break
        # Construct the history over leading hadron
        if parent1.pt() < parent2.pt():
            this_jet = parent2
        else:
            this_jet = parent1
    return kTdistances

def get_parents(input_jet,input_hist):
    if input_jet == fj.PseudoJet(0.0,0.0,0.0,0.0) or input_jet.pt() == 0.:
        return fj.PseudoJet(0.0,0.0,0.0,0.0), fj.PseudoJet(0.0,0.0,0.0,0.0), 0.
    parent1 = fj.PseudoJet(0.0,0.0,0.0,0.0)
    parent2 = fj.PseudoJet(0.0,0.0,0.0,0.0)
    kTdistance = 0.
    try:
        if input_hist.has_parents(input_jet,parent1,parent2):
            kTdistance = parent1.kt_distance(parent2)
            if parent1.pt() < parent2.pt():
                parent1, parent2 = parent2, parent1
    except:
        return parent1, parent2, kTdistance
    return parent1, parent2, kTdistance