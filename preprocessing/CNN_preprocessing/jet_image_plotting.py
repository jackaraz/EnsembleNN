#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 14:24:02 2019

@author: jackaraz
"""
import numpy as np
import matplotlib, os
import matplotlib.pyplot as plt
# Matplotlib configuration
#matplotlib.rcParams['text.usetex']        = True # can be turned on for python2.7
#matplotlib.rcParams['text.latex.unicode'] = True # can be turned on for python2.7
plt.rcParams['xtick.direction']           = 'in'
plt.rcParams['ytick.direction']           = 'in'
plt.rcParams['xtick.top']                 = True
plt.rcParams['xtick.minor.visible']       = True
plt.rcParams['ytick.right']               = True
plt.rcParams['ytick.minor.visible']       = True
plt.rcParams['xtick.labelsize']           = 12
plt.rcParams['ytick.labelsize']           = 12
plt.rcParams['axes.labelsize']            = 20

class fig_config(object):
    """Figure configuration class, eta span, phi span and pixel resolution"""
    def __init__(self,eta=[-5.,5],phi=[-np.pi,np.pi],pixels=[40,40], extent='obs'):
        self.eta_min     = eta[0]
        self.eta_max     = eta[1]
        self.phi_min     = phi[0]
        self.phi_max     = phi[1]
        if extent == 'obs':
            self.extent      = [eta[0],eta[1],phi[0],phi[1]]
        elif extent == 'pixel':
            self.extent      = [0,pixels[0],0,pixels[1]]
        else:
            raise Exception('extent can only be obs or pixel')
        self.nr_bins_eta = pixels[0]
        self.nr_bins_phi = pixels[1]
        self.d_eta       = round((self.eta_max-self.eta_min)/self.nr_bins_eta, 6)
        self.d_phi       = round((self.phi_max-self.phi_min)/self.nr_bins_phi, 6)

def quick_image_plot(image,config=fig_config(),N=-1):
    if N<0:
        N = len(image)
    for i in range(N):
        fig = plt.figure(figsize=(5,5))
        ax  = plt.gca()
        im  = plt.imshow(image[i].reshape(config.nr_eta_bins,config.nr_phi_bins),
                         interpolation='none',vmin=image[i].min(),vmax=image[i].max(),
                         extent=config.extent,#aspect=aspect_ratio,
                         cmap=matplotlib.cm.rainbow,
                         norm=None)
        plt.colorbar(im, fraction=0.0454, pad=0.05)
        ax.set_xlabel(r'$\eta$',fontsize=22)
        ax.set_ylabel(r'$\phi$',fontsize=22)
        ax.set_xlim(config.extent[:2])
        ax.set_ylim(config.extent[2:4])
        plt.minorticks_on()
        plt.show()
        plt.close('all')


def quick_image_plot_mean(image,config=fig_config()):
    fig = plt.figure(figsize=(5,5))
    ax  = plt.gca()
    image = np.array(image)
    image = image.mean(axis=0)
    im  = plt.imshow(image.reshape(config.nr_bins_eta,config.nr_bins_phi),
                     interpolation='gaussian',vmin=image.min(),vmax=image.max(),
                     extent=config.extent,aspect='auto',#10./(2*np.pi),
                     cmap=matplotlib.cm.rainbow,
                     norm=None)
    plt.colorbar(im, fraction=0.0454, pad=0.05)
    ax.set_xlabel(r'$\eta$',fontsize=22)
    ax.set_ylabel(r'$\phi$',fontsize=22)
    ax.set_xlim(config.extent[:2])
    ax.set_ylim(config.extent[2:4])
    plt.minorticks_on()
    plt.show()
    plt.close('all')


def comparison_plot(image_QCD,image_top,extent=[0,40,0,40],lim=[0,40,0,40], 
                    save_path=False, name='image'):
    fig, axarr = plt.subplots(1, 2, sharey=True)
    fig.set_size_inches(12, 6)
    axarr[0].imshow(np.array(image_QCD).mean(axis=0),extent=extent)
    axarr[1].imshow(np.array(image_top).mean(axis=0),extent=extent)  
    axarr[0].set_title('QCD')
    axarr[1].set_title(r'$t\bar{t}$')
    plt.tight_layout()
    axarr[0].set_xlim(lim[0:2])
    axarr[0].set_ylim(lim[2:4])
    axarr[1].set_xlim(lim[0:2])
    axarr[1].set_ylim(lim[2:4])
    if os.path.isdir(save_path):
        plt.savefig(os.path.join(save_path,name+'.png'), bbox_inches = 'tight')
        plt.close('all')
    else:
        plt.show()
