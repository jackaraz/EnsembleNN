#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 16:35:06 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

"""
Based on
     https://github.com/SebastianMacaluso/Deep-learning_jet-images/blob/master/image_preprocess.py
"""

#import os
import numpy as np
from EnsembleNN.system.feedback import *

def deltaphi(phi1,phi2):
    """
    Calculate azimuthal separation

    Parameters
    ----------
    phi1 : FLOAT
        Azimuthal angle.
    phi2 : FLOAT
        Azimuthal angle.

    Returns
    -------
    FLOAT
        returns the phi separation within [-2pi,2pi]

    """
    deltaphilist=[phi1-phi2,phi1-phi2+np.pi*2.,phi1-phi2-np.pi*2.]
    sortind=np.argsort(np.abs(deltaphilist))
    return deltaphilist[sortind[0]]


def center(Subjets):
    """
     center the image so that the total pT weighted centroid pixel is at 
     (eta,phi)=(0,0).

    Parameters
    ----------
    Subjets : LIST
        Subjets.

    Returns
    -------
    pTj : LIST
        Central pt of the subjets.
    eta_c : LIST
        Central eta of the subjets.
    phi_c : LIST
        Central phi of the subjets.

    """
    Njets=len(Subjets[0])
    pTj=[]
    for ijet in range(0,Njets):  
      pTj.append(np.sum(Subjets[0][ijet]))

    eta_c=[]
    phi_c=[]
    weigh_eta=[]
    weigh_phi=[]
    for ijet in range(0,Njets):
        weigh_eta.append([ ])
        weigh_phi.append([ ])
        for isubjet in range(0,len(Subjets[0][ijet])):
            weigh_eta[ijet].append(Subjets[0][ijet][isubjet]*Subjets[1][ijet][isubjet]/pTj[ijet]) 
            #We multiply pT by eta of each subjet
            weigh_phi[ijet].append(Subjets[0][ijet][isubjet]*deltaphi(Subjets[2][ijet][isubjet],Subjets[2][ijet][0])/pTj[ijet]) 
            #We multiply pT by phi of each subjet
        eta_c.append(np.sum(weigh_eta[ijet])) #Centroid value for eta
        phi_c.append(np.sum(weigh_phi[ijet])+Subjets[2][ijet][0]) #Centroid value for phi

    return pTj, eta_c, phi_c



def shift(Subjets,Eta_c,Phi_c):
    """
    Shift the coordinates of each particle so that the jet is centered at the 
    origin in (eta,phi) in the new coordinates

    Parameters
    ----------
    Subjets : LIST
        Subjets.
    eta_c : LIST
        Central eta of the subjets.
    phi_c : LIST
        Central phi of the subjets.

    Returns
    -------
    Subjets : TYPE
        centralized subjets.

    """
    Njets=len(Subjets[1])
    for ijet in range(0,Njets):
        Subjets[1][ijet]=(Subjets[1][ijet]-Eta_c[ijet])
        Subjets[2][ijet]=(Subjets[2][ijet]-Phi_c[ijet])
        Subjets[2][ijet]=np.unwrap(Subjets[2][ijet])
        #We fix the angle phi to be between (-Pi,Pi]
    return Subjets




def principal_axis(Subjets):
    """
    calculate the angle theta of the principal axis

    Parameters
    ----------
    Subjets : LIST
        Subjets.

    Returns
    -------
    tan_theta : LIST
        Principal axis.

    """
    tan_theta=[]#List of the tan(theta) angle to rotate to the principal axis in each jet image
    Njets=len(Subjets[1])
    for ijet in range(0,Njets):
        M11=np.sum(Subjets[0][ijet]*Subjets[1][ijet]*Subjets[2][ijet])
        M20=np.sum(Subjets[0][ijet]*Subjets[1][ijet]*Subjets[1][ijet])
        M02=np.sum(Subjets[0][ijet]*Subjets[2][ijet]*Subjets[2][ijet])
        tan_theta_use=2*M11/(M20-M02+np.sqrt(4*M11*M11+(M20-M02)*(M20-M02)))
        tan_theta.append(tan_theta_use)
    return tan_theta



def rotate(Subjets,tan_theta):
    """
    rotate the coordinate system so that the principal axis is the 
    same direction (+ eta) for all jets

    Parameters
    ----------
    Subjets : LIST
        Subjets.
    tan_theta : LIST
        Principal axis.

    Returns
    -------
    rot_subjet : LIST
        rotated subjets.

    """
    rot_subjet=[[],[],[]]
    Njets=len(Subjets[1])
    for ijet in range(0,Njets):
        rot_subjet[0].append(Subjets[0][ijet]) 
        rot_subjet[1].append(Subjets[1][ijet]*np.cos(np.arctan(tan_theta[ijet]))+Subjets[2][ijet]*np.sin(np.arctan(tan_theta[ijet])))
        rot_subjet[2].append(-Subjets[1][ijet]*np.sin(np.arctan(tan_theta[ijet]))+Subjets[2][ijet]*np.cos(np.arctan(tan_theta[ijet])))
        rot_subjet[2][ijet]=np.unwrap(rot_subjet[2][ijet]) #We fix the angle phi to be between (-Pi,Pi]
    return rot_subjet



def create_image(Subjets,DR=1.5,npoints = 38,treshold=0):
    """
    Create a coarse grid for the array of pT for the jet constituents, 
    where each entry represents a pixel. We add all the jet constituents 
    that fall within the same pixel 


    Parameters
    ----------
    Subjets : LIST
        Subjets.
    DR : FLOAT, optional
        DESCRIPTION. The default is 1.5.
    npoints : FLOAT, optional
        number of pixel+1. The default is 38.
    treshold : FLOAT, optional
        min subjet pt. The default is 0.

    Returns
    -------
    final_image : LIST
        images.
    Number_jets : INT
        number of images.

    """
    start_time=time.time()
    etamin, etamax = -DR, DR # Eta range for the image
    phimin, phimax = -DR, DR # Phi range for the image
    eta_i = np.linspace(etamin, etamax, npoints) #create an array with npoints elements between min and max
    phi_i = np.linspace(phimin, phimax, npoints)
    image=[]
    Njets=len(Subjets[0])
    for ijet in range(0,Njets):
        #progress(ijet,Njets,status='Creating images...',start_time=start_time)
        grid=np.zeros((npoints-1,npoints-1)) #create an array of zeros for the image 
        eta_idx = np.searchsorted(eta_i,Subjets[1][ijet]) 
        # np.searchsorted finds the index where each value in my data 
        #(Subjets[1] for the eta values) would fit into the sorted array eta_i 
        #(the x value of the grid).
        phi_idx = np.searchsorted(phi_i,Subjets[2][ijet])
        # np.searchsorted finds the index where each value in my data 
        #(Subjets[2] for the phi values) would fit into the sorted array phi_i 
        #(the y value of the grid).
        for pos in range(0,len(eta_idx)):
            if eta_idx[pos]!=0 and phi_idx[pos]!=0 and eta_idx[pos]<npoints and phi_idx[pos]<npoints: 
                #If any of these conditions are not true, then that jet constituent is not included in the image. 
                grid[eta_idx[pos]-1,phi_idx[pos]-1]=grid[eta_idx[pos]-1,phi_idx[pos]-1]+Subjets[0][ijet][pos] 
                #We add each subjet pT value to the right entry in the grid to create the image. As the values of 
                #(eta,phi) should be within the interval (eta_i,phi_i) of the image, 
                #the minimum eta_idx,phi_idx=(1,1) to be within the image. 
                #However, this value should be added to the pixel (0,0) in the grid. 
                #That's why we subtract 1.
        sum=np.sum(grid)
        if sum>=treshold:
            image.append(grid)

    Number_jets=len(image)
    final_image=image

    return final_image, Number_jets


def flip(Image,Nimages,npoints = 38): 
    """
    Reflect the image with respect to the vertical axis to ensure the 3rd maximum is on the right half-plane


    Parameters
    ----------
    Image : TYPE
        DESCRIPTION.
    Nimages : TYPE
        DESCRIPTION.
    npoints : TYPE, optional
        DESCRIPTION. The default is 38.

    Returns
    -------
    flip_image : TYPE
        DESCRIPTION.

    """
    count=0
    start_time=time.time()
    half_img=np.int((npoints-1)/2)
    flip_image=[]
    for i_image in range(len(Image)):
        progress(i_image,len(Image),status='Vertical flip...',start_time=start_time)
        left_img=[] 
        right_img=[]
        for i_row in range(np.shape(Image[i_image])[0]):
            left_img.append(Image[i_image][i_row][0:half_img])
            right_img.append(Image[i_image][i_row][-half_img:])

        left_sum=np.sum(left_img)
        right_sum=np.sum(right_img)

        if left_sum>right_sum:
            flip_image.append(np.fliplr(Image[i_image]))     
        else:
            flip_image.append(Image[i_image])
            count+=1
    return flip_image  


def hor_flip(Image,Nimages,npoints = 38):
    """
    Reflect the image with respect to the horizontal axis to ensure the 3rd maximum is on the top half-plane


    Parameters
    ----------
    Image : TYPE
        DESCRIPTION.
    Nimages : TYPE
        DESCRIPTION.
    npoints : TYPE, optional
        DESCRIPTION. The default is 38.

    Returns
    -------
    hor_flip_image : TYPE
        DESCRIPTION.

    """
    count=0
    start_time=time.time()
    half_img=np.int((npoints-1)/2)
    hor_flip_image=[]
    for i_image in range(len(Image)):
        progress(i_image,len(Image),status='Horizontal flip...',start_time=start_time)
        top_img=[] 
        bottom_img=[]
        for i_row in range(half_img):
            top_img.append(Image[i_image][i_row])
            bottom_img.append(Image[i_image][-i_row-1])
        top_sum=np.sum(top_img)
        bottom_sum=np.sum(bottom_img)

        if bottom_sum>top_sum:
            hor_flip_image.append(np.flip(Image[i_image],axis=0))     
        else:
            hor_flip_image.append(Image[i_image])
            count+=1
    return hor_flip_image


def preprocess(subjets):
    """
    PREPROCESS IMAGES 
    (center, shift, principal_axis, rotate, vertical flip, horizontal flip)
    leaving normalization for standardization.

    Parameters
    ----------
    subjets : TYPE
        DESCRIPTION.

    Returns
    -------
    hor_flipped_img : TYPE
        DESCRIPTION.

    """
    pTj, eta_c, phi_c=center(subjets)  
    shift_subjets=shift(subjets,eta_c,phi_c)
    tan_theta=principal_axis(shift_subjets) 
    rot_subjets=rotate(shift_subjets,tan_theta)
    raw_image, Nimages=create_image(rot_subjets)  
    ver_flipped_img=flip(raw_image,Nimages)  
    hor_flipped_img=hor_flip(ver_flipped_img,Nimages)  

    return hor_flipped_img



def normalize(image,method=1000.):
    new_image = []
    if type(method)==float:
        for im in image:
            new_image.append((np.array(im)/method).tolist())
    elif method=='sum':
        for im in image:
            new_image.append((np.array(im)/np.array(im).sum()).tolist())
    elif method =='max':
        for im in image:
            new_image.append((np.array(im)/np.array(im).max()).tolist())
    return new_image



# C) GET STANDARD DEVIATION OF A SET OF IMAGES
def get_std(Image,method='std',bias=1e-02, N_pixels=np.power(38-1,2),npoints = 38):
    import scipy
    Image_row=[]
    for i_image in range(len(Image)):
        Image_row.append(Image[i_image].reshape((N_pixels)))
    Image_row=np.array(Image_row,dtype=np.float64)
    Image_row.reshape((len(Image),N_pixels))
    if method=='n_moment':
        n_moment=scipy.stats.moment(Image_row, moment=4, axis=0)
        standard_dev=np.std(Image_row,axis=0,ddof=1, dtype=np.float32)
        final_bias=np.power(n_moment,1/4)+bias
    elif method=='std':  
        standard_dev=np.std(Image_row,axis=0,ddof=1, dtype=np.float32)
        final_bias=standard_dev+bias
    final_bias=final_bias.reshape((npoints-1,npoints-1))
    return final_bias


# D) USE STANDARD DEVIATION FROM ANOTHER SET OF IMAGES
def standardize_bias_std_other_set(Image, input_std_bias,npoints = 38): 
    std_im_list=[]
    for i_image in range(len(Image)):
        std_im_list.append(Image[i_image]/input_std_bias)
        std_im_list[i_image]=std_im_list[i_image].reshape((npoints-1,npoints-1))
    return std_im_list



# E) PUT ALL TOGETHER  
def standardize_images(images,reference_images,method):
    out_std_bias=get_std(reference_images, method)
    standard_image=standardize_bias_std_other_set(images,out_std_bias)
    return standard_image





