#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 13:19:18 2020

@author  : jackaraz
@contact : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np

class ImageIterator:
    def __init__(self,filename, start=0,iter_size=10000):
        self.data_file = np.load(filename)
        if type(self.data_file) != type(np.array([])):
            if 'image' not in self.data_file.keys():
                raise Warning('Wrong type of input: '+filename)
            else:
                self.type = 'image'
        else:
            self.type = 'array'
        self.start     = start
        self.iter_size = iter_size

    def __iter__(self):
        return self

    def __next__(self):
        """
            This process is not fast it will read the file with each iteration
            so that it wont upload all
        """
        if self.type == 'image':
            data = self.data['image']
        else:
            data = self.data_file
        if self.start > data.shape[0]:
            raise StopIteration
        lower = self.start
        if lower+self.iter_size < data.shape[0]:
            upper = lower+self.iter_size
        else:
            upper = data.shape[0]-1
        self.start = upper+1
        return data[lower:upper]


class ImageIterator2D:
    def __init__(self,filename1,filename2, start=0,iter_size=10000):
        self.data_file1 = np.load(filename1)
        self.data_file2 = np.load(filename2)
        self.start      = start
        self.iter_size  = iter_size

    def __iter__(self):
        return self

    def __next__(self): # next(self) for python2
        """
            This process is not fast it will read the file with each iteration
            so that it wont upload all
        """
        if 'shape' in self.data_file1.keys() and 'shape' in self.data_file2.keys():
            data_shape = min(self.data_file1['shape'][0], self.data_file2['shape'][0])
            if self.start < data_shape:
                lower = self.start
                if lower+self.iter_size < data_shape:
                    upper = lower+self.iter_size
                else:
                    upper = data_shape-1
                self.start = upper+1
                return self.data_file1['image'][lower:upper], self.data_file2['image'][lower:upper]
        else:
            data1 = self.data_file1['image']
            data2 = self.data_file2['image']
            data_shape = min(data1.shape[0], data2.shape[0])
            if self.start < data_shape:
                lower = self.start
                if lower+self.iter_size < data_shape:
                    upper = lower+self.iter_size
                else:
                    upper = data_shape-1
                self.start = upper+1
                return data1[lower:upper], data2[lower:upper]
        raise StopIteration

