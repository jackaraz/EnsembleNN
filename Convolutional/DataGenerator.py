#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 14:15:58 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np
np.warnings.filterwarnings('ignore',category=np.VisibleDeprecationWarning)
from tensorflow.keras.utils import Sequence
import os

"""
    https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
    https://machinelearningmastery.com/how-to-load-large-datasets-from-directories-for-deep-learning-with-keras/
    Generates data for Keras
"""

class DataSample:
    def __init__(self,data_tuple):
        self.X_CovNet = []
        self.y        = []
        self.__reset_iteration()
        for cov, truth in data_tuple:
            self.X_CovNet.append(cov)
            self.y.append(truth)

    def __iter__(self):
        self.__reset_iteration()
        return self

    def __next__(self):
        try:
            if self.n < len(self.y):
                result = (self.X_CovNet[self.n], self.y[self.n])
                self.n += 1
                return result
            else:
                return (-1,-1)
        except:
            return (-1,-1)

    def next(self): # for py2.7
        return self.__next__()

    def __reset_iteration(self):
        self.n = 0



class DataGenerator(Sequence):
    def __init__(self,smp_path_dict,batch_size=32, shuffle=True,test=False,
                 std=None):
        """
        Parameters
        ----------
        smp_path_dict : Dictionary
            Needs to be a dictionary including full arranged paths of all signal and 
            background samples.
            smp_path_dict = {'image' : {'sig': [LIST OF SIGNAL SMP], 'bkg': [LIST OF BKG SMP]}}
        batch_size : INT, optional
            Size of the batch to be returned. The default is 32.
        shuffle : BOOL, optional
            Should the sample be shuffled. The default is True.
        test : BOOL, optional
            Is this a test sample in that case code will save truth values. 
            The default is False.

        Returns
        -------
        None.
        """
        self.image_paths = smp_path_dict['image']
        self.batch_size  = batch_size
        self.shuffle     = shuffle
        self.test        = test
        self.__initialize_samples()
        self.size        = self.__len__()

        assert (std == None or hasattr(std,'__call__')),\
                'Standardization has to be either a function or None.'
        self.std         = std
        self.init        = True
        self.datasample  = []
        # self.on_epoch_end()

    def Reset(self):
        self.datasample  = []
        self.init        = True
        self.nfile       = 0


    def __initialize_samples(self):
        ' Prepare sample containers, they need to be ordered in advance'
        if 'samples' not in self.__dict__.keys() and all([(x in ['sig','bkg']) for x in self.image_paths.keys()]):
            self.sample_size = 0
            self.signal_size = 0
            self.bkg_size    = 0
            file_set = []
            for fl_sig, fl_bkg in zip(self.image_paths['sig'],self.image_paths['bkg']):
                if not all([os.path.isfile(x) for x in [fl_sig, fl_bkg]]):
                    continue

                nsig = np.load(fl_sig)['shape'][0]
                nbkg = np.load(fl_bkg)['shape'][0]
    
                self.sample_size += nsig+nbkg
                self.signal_size += nsig
                self.bkg_size    += nbkg

                file_set.append((fl_sig, fl_bkg))

            self.samples = file_set


    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size


    def __get_current_file_set(self):
        return self.samples[self.nfile]


    def __getitem__(self,index):
        """
        Parameters
        ----------
        index : INT
            Index not important just index < len(DataGenerator).

        Returns
        -------
        TUPLE or LIST
            If this is a test sample a list of Xs will be returned,
            but if this is a train sample a tuple containing list of Xs and 
            y will be returned.
        """
        # assert index < len(self) , 'index out of range...'
        if self.init:
            self.on_epoch_end()
            self.init = False

        # Generate data
        X_CovNet, y = self.__data_generation()

        if not self.test:
            return X_CovNet, y
        else:
            if '_y' not in list(self.__dict__.keys()):
                self._y = []
            self._y += y.tolist()
            return X_CovNet


    def y_test(self):
        if '_y' in list(self.__dict__.keys()):
            return self._y
        else:
            return


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.nfile = 0
        if self.shuffle == True:
            np.random.shuffle(self.samples)
        self.__read_data()


    def __read_data(self):
        sgim, bkim = self.__get_current_file_set()
        signal_image = np.load(sgim)['sample']
        bkg_image    = np.load(bkim)['sample']

        min_size_sig = signal_image.shape[0]
        min_size_bkg = bkg_image.shape[0]

        y = []
        y.extend([1]*min_size_sig)
        y.extend([0]*min_size_bkg)

        images = np.vstack((signal_image[:min_size_sig], bkg_image[:min_size_bkg]))
        images = images if self.std == None else self.std(images)
        images = images.reshape(images.shape[0],images.shape[1],images.shape[2],1)

        data_tuple = list(zip(images,y))
        if self.shuffle:
            np.random.shuffle(data_tuple)
        data_tuple = np.array(data_tuple)
        self.datasample = iter(DataSample(data_tuple))


    def __data_generation(self):
        'Generates data containing batch_size samples' 
        # Initialization
        # Get file names
        Xcov, y = [],[]
        while len(y) < self.batch_size:
            xcov, ytruth = next(self.datasample)
            if not (isinstance(xcov,np.ndarray) and (ytruth in [0,1])):
                self.nfile +=1
                if self.nfile == len(self.samples):
                    self.on_epoch_end()
                else:
                    self.__read_data()
                continue
            elif xcov.shape != (37,37,1):
                continue
            else:
                Xcov.append(xcov)
                y.append(ytruth)
        return np.array(Xcov), np.array(y)
