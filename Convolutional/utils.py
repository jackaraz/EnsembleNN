#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 14:51:29 2020

@author : Jack Y. Araz <jackaraz@gmail.com>
"""

import numpy as np

def get_correlations(images, yhat):
    """
    calculate linear correlation between each pixel and the output of the classifier
    to see what pixels are more indicative of a specific class.
    """
    import pandas as pd
    # -- find the total number of pixels per image, here 25 x 25
    n_pixels = np.prod(images.shape[1:3])
    
    # -- add the pixels as columns to a dataframe
    df = pd.DataFrame(
        {i : np.squeeze(images).reshape(-1, n_pixels)[:, i] for i in range(n_pixels)}
    )
    # -- add a column to the end of the dataframe for the discriminator's output
    df['disc_output'] = yhat
    # -- pandas offers an easy solution to calculate correlations 
    # (even though it's slow because it also calculates the correlation between each pixel and every other pixel)
    correlations = df.corr().values[:-1, -1]
    return correlations


def get_error_correlations(images, yhat, ytrue):
    """
    calculate linear error correlation between each pixel and the output of the classifier
    to see what pixels are more indicative of a specific class.
    """
    import pandas as pd
    # -- find the total number of pixels per image, here 25 x 25
    n_pixels = np.prod(images.shape[1:3])
    
    # -- add the pixels as columns to a dataframe
    df = pd.DataFrame(
        {i : np.squeeze(images).reshape(-1, n_pixels)[:, i] for i in range(n_pixels)}
    )
    # -- add a column to the end of the dataframe for the discriminator's error
    ytrue = ytrue.reshape(yhat.shape)
    df['disc_output'] = (ytrue - yhat)**2
    # -- pandas offers an easy solution to calculate correlations 
    # (even though it's slow because it also calculates the correlation between each pixel and every other pixel)
    correlations = df.corr().values[:-1, -1]
    return correlations


def plot_correlations(correlations, extent, title='', img_dim=(37, 37), 
                      name='corr.png',cbar_label=r"Correlation Magnitude"):
    """
    call the function about and then plot the correlations in image format
    """
    import matplotlib.pyplot as plt
    plt.rcParams['xtick.direction']           = 'in'
    plt.rcParams['ytick.direction']           = 'in'
    plt.rcParams['xtick.top']                 = True
    plt.rcParams['xtick.minor.visible']       = True
    plt.rcParams['ytick.right']               = True
    plt.rcParams['ytick.minor.visible']       = True
    plt.rcParams['xtick.labelsize']           = 12
    plt.rcParams['ytick.labelsize']           = 12
    plt.rcParams['axes.labelsize']            = 20
    from matplotlib.colors import Normalize
    max_mag = max(
        abs(np.min(correlations[np.isfinite(correlations)])),
        abs(np.max(correlations[np.isfinite(correlations)])),
    ) # highest correlation value (abs value), to make the plot look nice and on a reasonable scale

    fig = plt.figure(1, figsize=(8, 6), facecolor='w', edgecolor='k')
    sys = fig.add_subplot(111)
    im = sys.imshow(
        correlations.reshape(img_dim),
        interpolation='gaussian',
        norm=Normalize(vmin=np.min(correlations[np.isfinite(correlations)]), vmax=max_mag),
        extent=extent,
        cmap=plt.cm.seismic
    )
    cbar = plt.colorbar(im)
    cbar.set_label(cbar_label, fontsize=20)
    #plt.colorbar(im, fraction=0.05, pad=0.05)
    plt.xlabel("$\eta^\prime$",fontsize=20)
    plt.ylabel("$\phi^\prime$",fontsize=20)
    plt.xlim([-.7,.7])
    plt.ylim([-.7,.7])
    if name == '':
        plt.show()
    else:
        plt.savefig(name, bbox_inches = 'tight')